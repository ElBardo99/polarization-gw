#!/usr/bin/env python
# coding: utf-8

# In[39]:


#from numba import jit, cuda
import multiprocessing
print("Number of cpu : ", multiprocessing.cpu_count())
import time
start=time.time()
from timeit import default_timer as timer 
import sys, os
sys.stdout.flush()
import numpy as np
from gwbench import network
from gwbench import basic_relations as br
import matplotlib.pyplot as plt
#from gwbench import io_mod as io
import scipy
#import scipy.signal
from gwbench.basic_constants import Mpc, Msun, GNewton, cLight
def ChirpTime(fs, Mc):
    Mc *= GNewton/cLight**3 * Msun
    fn = np.asarray([fs]) if np.isscalar(fs) else np.asarray(fs)
    return ((5./256.) / (Mc**(5./3.) * (np.pi * fn)**(8./3.)))
def setup_plot(figsize, xlabel, ylabel):
    """ @brief Initialize basic plot parameters
        @param figsize Figure size
        @param xlabel X-axis label
        @param ylabel Y-axis label
    """

    plt.figure(figsize=figsize)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

from scipy.integrate import quad
in1 =20.
distanza_l= in1
Mc=35
def integr(zi):
    Ho=68.0
    c=300000.0
    Om=0.3
    Ok=0.0
    res=(c/Ho)*1/(np.sqrt(Om*(1+zi)**3 +  1-Om))
    return res
redshifted=1
file = open("gwbench/noise_curves/a_plus.txt", "r")

arr_str=str(file.read()).split("\n")
#print(arr_str[0])
for i in range (0, len(arr_str)):
    arr_str[i]=arr_str[i].split(' ')
arr_str.pop()
for i in range (0, len(arr_str)):
    #print(i)
    for j in (0, 1):
        arr_str[i][j]=float(arr_str[i][j])
arr_str=np.array(arr_str)
#print(arr_str)
noise1=np.transpose(arr_str)

#noise1[0,0]=5

file = open("gwbench/noise_curves/a_plus.txt", "r")

arr_str=str(file.read()).split("\n")
#print(arr_str[0])
for i in range (0, len(arr_str)):
    arr_str[i]=arr_str[i].split(' ')
arr_str.pop()
for i in range (0, len(arr_str)):
    #print(i)
    for j in (0, 1):
        arr_str[i][j]=float(arr_str[i][j])
arr_str=np.array(arr_str)
#print(arr_str)
noise2=np.transpose(arr_str)

#noise2[0,0]=5

file = open("gwbench/noise_curves/ce2_20km_cb.txt", "r")

arr_str=str(file.read()).split("\n")
#print(arr_str[0])
for i in range (0, len(arr_str)):
    arr_str[i]=arr_str[i].split(' ')
arr_str.pop()
for i in range (0, len(arr_str)):
    #print(i)
    for j in (0, 1):
        arr_str[i][j]=float(arr_str[i][j])
arr_str=np.array(arr_str)
#print(arr_str)
noise3=np.transpose(arr_str)
arr_str=None
#noise3[0,0]=3






cos = np.cos
sin = np.sin
exp = np.exp
PI = np.pi
from gwbench.basic_constants import time_fac, REarth, AU, cLight

def rrot_mat(angle,axis):
    c = np.cos(angle)
    s = np.sin(angle)

    if axis == 'i':
        return np.array( ( (1,0,0), (0,c,-s), (0,s,c) ) )
    if axis == 'j':
        return np.array( ( (c,0,s), (0,1,0), (-s,0,c) ) )
    if axis == 'k':
        return np.array( ( (c,-s,0), (s,c,0), (0,0,1) ) )

def ddet_angles(loc):
    # return alpha, beta, gamma in radians
    # alpha ... longitude
    # beta  ... pi/2 - latitude
    # gamma ... angle from 'Due East' to y-arm
    if loc == 'H':
        return -2.08406, PI/2.-0.810795, PI-5.65488
    elif loc == 'L':
        return -1.58431, PI/2.-0.533423, PI-4.40318
    elif loc in ('V','ET1','ET2','ET3'):
        return 0.183338, PI/2.-0.761512, PI-0.33916
    elif loc == 'K':
        return 2.3942, PI/2.-0.632682, PI-1.054113
    elif loc == 'I':
        return 1.334013, PI/2.-0.248418, PI-1.570796

    elif loc == 'C':
        return -1.969174, PI/2.-0.764918, 0.
    elif loc == 'N':
        return -2.1817, PI/2.-0.8029, 3*PI/4.
    elif loc == 'S':
        return -1.64061, PI/2.-0.50615, 0.


def ddet_ten_and_loc_vec(loc, R):
    i_vec = np.array((1,0,0))
    j_vec = np.array((0,1,0))
    k_vec = np.array((0,0,1))

    et_vec2 = ( i_vec + np.sqrt(3.)*j_vec)/2.
    et_vec3 = (-i_vec + np.sqrt(3.)*j_vec)/2.

    alpha, beta, gamma = ddet_angles(loc)
    EulerD1 = np.matmul(np.matmul(rrot_mat(alpha,'k'), rrot_mat(beta,'j')), rrot_mat(gamma,'k'))

    if loc in   ('ET3','LISA3'):
        eDArm1 = -1 * np.matmul(EulerD1,et_vec2)
        eDArm2 = -1 * np.matmul(EulerD1,et_vec3)
    elif loc in ('ET2','LISA2'):
        eDArm1 =      np.matmul(EulerD1,et_vec3)
        eDArm2 = -1 * np.matmul(EulerD1,i_vec)
    elif loc in ('ET1','LISA1'):
        eDArm1 =      np.matmul(EulerD1,i_vec)
        eDArm2 =      np.matmul(EulerD1,et_vec2)
    else:
        eDArm1 = np.matmul(EulerD1,i_vec)
        eDArm2 = np.matmul(EulerD1,j_vec)

    return np.outer(eDArm1,eDArm1) - np.outer(eDArm2,eDArm2), R/cLight * np.matmul(EulerD1,k_vec)

def apkons(ra,dec,t,Mc,tc,psi,gmst0,loc,use_rot, time):
    half_period = 4.32e4
    R = REarth

    D, d = ddet_ten_and_loc_vec(loc, R)

    if use_rot:
        if time:
            tf = t
        else:
            tf= tc - (5./256.)*(time_fac*Mc)**(-5./3.)*(PI*t)**(-8./3.)
    else:
        tf = 0

    gra = (gmst0 + tf*PI/half_period) - ra
    #print(type(gra))
    theta = PI/2. - dec

    if isinstance(gra, np.ndarray):
        r = np.array((cos(gra) * sin(theta), sin(gra) * sin(theta), cos(theta) * np.ones(len(gra))))
        XX = np.transpose(np.array([ -cos(psi)*sin(gra) - sin(psi)*cos(gra)*sin(dec), -cos(psi)*cos(gra) + sin(psi)*sin(gra)*sin(dec), sin(psi)*cos(dec) * np.ones(len(gra)) ]))
        YY = np.transpose(np.array([  sin(psi)*sin(gra) - cos(psi)*cos(gra)*sin(dec),  sin(psi)*cos(gra) + cos(psi)*sin(gra)*sin(dec), cos(psi)*cos(dec) * np.ones(len(gra)) ]))
        ZZ = np.transpose(np.array([  -cos(gra)*cos(dec) , cos(dec)*sin(gra) , -sin(dec)* np.ones(len(gra))]))
        Fp = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),XX[i]) - np.matmul(np.matmul(YY[i],D),YY[i]) for i in range(len(gra))])
        Fc = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),YY[i]) + np.matmul(np.matmul(YY[i],D),XX[i]) for i in range(len(gra))])
        Fs1 = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),XX[i]) + np.matmul(np.matmul(YY[i],D),YY[i]) for i in range(len(gra))])
        Fs2 = 0.5 * np.array([np.matmul(np.matmul(ZZ[i],D),ZZ[i]) for i in range(len(gra))])
        Fv = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),ZZ[i]) + np.matmul(np.matmul(ZZ[i],D),XX[i]) for i in range(len(gra))])
        Fw = 0.5 * np.array([np.matmul(np.matmul(YY[i],D),ZZ[i]) + np.matmul(np.matmul(YY[i],D),ZZ[i]) for i in range(len(gra))])
    else:
        r = np.array((cos(gra) * sin(theta), sin(gra) * sin(theta), cos(theta)))
        XX = np.transpose(np.array([ -cos(psi)*sin(gra) - sin(psi)*cos(gra)*sin(dec), -cos(psi)*cos(gra) + sin(psi)*sin(gra)*sin(dec), sin(psi)*cos(dec) ]))
        YY = np.transpose(np.array([  sin(psi)*sin(gra) - cos(psi)*cos(gra)*sin(dec),  sin(psi)*cos(gra) + cos(psi)*sin(gra)*sin(dec), cos(psi)*cos(dec) ]))
        ZZ = np.transpose(np.array([  -cos(gra)*cos(dec) , cos(dec)*sin(gra) , -sin(dec)]))
        Fp = 0.5 * (np.matmul(np.matmul(XX,D),XX) - np.matmul(np.matmul(YY,D),YY))
        Fc = 0.5 * (np.matmul(np.matmul(XX,D),YY) + np.matmul(np.matmul(YY,D),XX))
        Fs1 = 0.5 * (np.matmul(np.matmul(XX,D),XX) + np.matmul(np.matmul(YY,D),YY))
        Fs2 = 0.5 * (np.matmul(np.matmul(ZZ,D),ZZ))
        Fv = 0.5 * (np.matmul(np.matmul(XX,D),ZZ) + np.matmul(np.matmul(ZZ,D),XX))
        Fw = 0.5 * (np.matmul(np.matmul(YY,D),ZZ) + np.matmul(np.matmul(ZZ,D),YY))
    #print(len(Fp))
    return Fp, Fc, Fv , Fw , Fs1 , Fs2

bool_scal=1
bool_vec=0
As=1
Aw=1
Av=1
use_rot=1



network_spec3=['ET_ET1','ET_ET2','ET_ET3','CE2-20-CBO_N', 'CE2-40-CBO_S']
network_spec2 = ['A+_I','A+_H', 'A+_L',  'A+_V', 'A+_K']
network_spec1 = ['A+_H', 'A+_L',  'A+_V', 'A+_K']



# In[40]:


Mc_bbh = 35.0

Mc_bbhf = Mc_bbh

etazonz=0.249
redshifted=1


def fun_bbh(z):

    Lsky=quad(integr,0,z,args=())[0]
    zer=distanza_l - (1+z)*Lsky
    return zer

if redshifted:
    z_vec_bbh=scipy.optimize.root_scalar(fun_bbh, bracket=[0.0, 3]).root
    print(z_vec_bbh)
    
    Mc_bbh *= (1. + z_vec_bbh)
    

net3=network.Network(network_spec3)
net2=network.Network(network_spec2)
net1=network.Network(network_spec1)
wf_model_name = 'tf2'
net2.set_wf_vars(wf_model_name='tf2')
net3.set_wf_vars(wf_model_name='tf2')
net1.set_wf_vars(wf_model_name='tf2')


inj_params_bbh = {
    'Mc':    Mc_bbh,
    'eta':   etazonz,
    'chi1z': 0,
    'chi2z': 0,
    'DL':    distanza_l,
    'tc':    0,
    'phic':  0,
    'iota':  np.pi/4,
    'ra':    np.pi/4,
    'dec':   np.pi/4,
    'psi':   np.pi/4,
    'gmst0': 0
}

#Mtot=br.M_of_Mc_eta(35,0.24)
p=1
#Mtot_td=br.M_of_Mc_eta(inj_params['Mc'],inj_params['eta'])

f_lo1=20.
f_lo2=20.
f_lo3=3.
f_hi = br.f_isco_Msolar(br.M_of_Mc_eta(inj_params_bbh['Mc'], inj_params_bbh['eta']))
tc1 = ChirpTime(f_lo1, inj_params_bbh['Mc'])
print('Chirp Time BBH = ', tc1)
sys.stdout.flush()
tc2 = ChirpTime(f_lo2, inj_params_bbh['Mc'])
print('Chirp Time BNS = ', tc2)
sys.stdout.flush()
tc3 = ChirpTime(f_lo3, inj_params_bbh['Mc'])
print('Chirp Time BNS = ', tc3)
sys.stdout.flush()

f_hi1=4096.0/2.0
deltaT = 1/(2*f_hi1)
nts_bbh1 = int(2**(np.ceil(np.log2(tc1/deltaT))+p))
nts_bbh2 = int(2**(np.ceil(np.log2(tc2/deltaT))+p))
nts_bbh3 = int(2**(np.ceil(np.log2(tc3/deltaT))+p))
#df_bbh = 1./(nts_bbh*deltaT)
df1 , df2, df3 = 2**-4 , 2**-4 , 2**-4
f3=np.arange(f_lo3, f_hi, df3)
t3=np.linspace(-tc3, 0, 2*int(f_hi1/df3)-1)
f2=np.arange(f_lo2, f_hi, df2)
t2=np.linspace(-tc2, 0, 2*int(f_hi1/df2)-1)
f1=np.arange(f_lo1, f_hi, df1)
t1=np.linspace(-tc1, 0, 2*int(f_hi1/df1)-1)


dets_try1={
    'A+_H': 0,
    'A+_L': 1,
    'A+_V': 2,
    'A+_K': 3

}
dets_try2={
    'A+_I': 0,
    'A+_H': 1,
    'A+_L': 2,
    'A+_V': 3,
    'A+_K': 4
}
dets_try3={
    'ET_ET1': 0,
    'ET_ET2': 1,
    'ET_ET3': 2,
    'CE2-20-CBO_N': 3,
    'CE2-40-CBO_S': 4
}




deriv_symbs_string = 'Mc eta DL chi1z chi2z tc phic iota ra dec psi'
conv_cos = ('')
conv_log = ('')
use_rot = 1

net1.set_net_vars(
    f=f1, inj_params=inj_params_bbh,
    deriv_symbs_string=deriv_symbs_string,
    conv_cos=conv_cos, conv_log=conv_log,
    use_rot=use_rot
    )

net1.calc_wf_polarizations()
#net_bbh.calc_wf_polarizations_derivs_num()
net1.setup_ant_pat_lpf_psds()
net1.calc_det_responses()
#net_bbh.calc_det_responses_derivs_num()
#net_bbh.calc_snrs()
#net_bbh.calc_errors(cond_sup=np.inf)
#net_bbh.calc_sky_area_90()



net2.set_net_vars(
    f=f2, inj_params=inj_params_bbh,
    deriv_symbs_string=deriv_symbs_string,
    conv_cos=conv_cos, conv_log=conv_log,
    use_rot=use_rot
    )

net2.calc_wf_polarizations()
#net_bbh.calc_wf_polarizations_derivs_num()
net2.setup_ant_pat_lpf_psds()
net2.calc_det_responses()
#net_bbh.calc_det_responses_derivs_num()
#net_bbh.calc_snrs()
#net_bbh.calc_errors(cond_sup=np.inf)
#net_bbh.calc_sky_area_90()




net3.set_net_vars(
    f=f3, inj_params=inj_params_bbh,
    deriv_symbs_string=deriv_symbs_string,
    conv_cos=conv_cos, conv_log=conv_log,
    use_rot=use_rot
    )

net3.calc_wf_polarizations()
#net_bbh.calc_wf_polarizations_derivs_num()
net3.setup_ant_pat_lpf_psds()
net3.calc_det_responses()
#net_bbh.calc_det_responses_derivs_num()
#net_bbh.calc_snrs()
#net_bbh.calc_errors(cond_sup=np.inf)
#net_bbh.calc_sky_area_90()


data1=[0.0]*4
data2=[0.0]*5
data3=[0.0]*5
for dec in net1.detectors:
    for det in dets_try1:

        if '_'.join((str(dec.tec), str(dec.loc)))==str(det):
           # print(int(dets_try[det]))
            data1[int(dets_try1[det])]=np.array(dec.hf/dec.Flp, dtype=complex)
            #print(type(dec.hf))
            #p1[int(dets_try1[det])]=dec.Fp
            #c1[int(dets_try1[det])]=dec.Fc

for dec in net2.detectors:
    for det in dets_try2:

        if '_'.join((str(dec.tec), str(dec.loc)))==str(det):
            #print(int(dets_try[det]))
            data2[int(dets_try2[det])]=np.array(dec.hf/dec.Flp, dtype=complex)
            #print(type(dec.hf))
            #p2[int(dets_try2[det])]=dec.Fp
            #c2[int(dets_try2[det])]=dec.Fc

            
for dec in net3.detectors:
    for det in dets_try3:

        if '_'.join((str(dec.tec), str(dec.loc)))==str(det):
            #print(int(dets_try[det]))
            data3[int(dets_try3[det])]=np.array(dec.hf/dec.Flp, dtype=complex)
            #print(type(dec.hf))
            #p3[int(dets_try3[det])]=dec.Fp
            #c3[int(dets_try3[det])]=dec.Fc

sys.stdout.flush()


fre1_td1=np.arange(0, f_hi1, df1)
fre1_td2=np.arange(0, f_hi1, df2)
fre1_td3=np.arange(0, f_hi1, df3)
hplusf1=net1.hfp
hcrossf1=net1.hfc
net1=None
hplusf2=net2.hfp
hcrossf2=net2.hfc
net2=None
hplusf3=net3.hfp
hcrossf3=net3.hfc
net3=None
hplusf1=np.pad(hplusf1, (int(f_lo1/df1),int((f_hi1-f_hi)/df1)))
hcrossf1=np.pad(hcrossf1, (int(f_lo1/df1),int((f_hi1-f_hi)/df1)))
hplusf2=np.pad(hplusf2, (int(f_lo2/df2),int((f_hi1-f_hi)/df2)))
hcrossf2=np.pad(hcrossf2, (int(f_lo2/df2),int((f_hi1-f_hi)/df2)))
hplusf3=np.pad(hplusf3, (int(f_lo3/df3),int((f_hi1-f_hi)/df3)))
hcrossf3=np.pad(hcrossf3, (int(f_lo3/df3),int((f_hi1-f_hi)/df3)))



hgrfp1=hplusf1/((5./2.)*(1+np.cos(inj_params_bbh['iota'])**2))
hgrfc1=-complex(0,-1)*hcrossf1/(5.*np.cos(inj_params_bbh['iota']))
hgrfp2=hplusf2/((5./2.)*(1+np.cos(inj_params_bbh['iota'])**2))
hgrfc2=-complex(0,-1)*hcrossf2/(5.*np.cos(inj_params_bbh['iota']))
hgrfp3=hplusf3/((5./2.)*(1+np.cos(inj_params_bbh['iota'])**2))
hgrfc3=-complex(0,-1)*hcrossf3/(5.*np.cos(inj_params_bbh['iota']))


hscalf1=As*hgrfp1*(np.sin(inj_params_bbh['iota']))**2*np.sqrt(225./8.)
hscalf2=As*hgrfp2*(np.sin(inj_params_bbh['iota']))**2*np.sqrt(225./8.)
hscalf3=As*hgrfp3*(np.sin(inj_params_bbh['iota']))**2*np.sqrt(225./8.)
hwf1=Aw*np.sqrt(15./2.)*np.sin(inj_params_bbh['iota'])*hgrfp1
hwf2=Aw*np.sqrt(15./2.)*np.sin(inj_params_bbh['iota'])*hgrfp2
hwf3=Aw*np.sqrt(15./2.)*np.sin(inj_params_bbh['iota'])*hgrfp3
hvf1=Av*np.sqrt(525./56.)*np.sin(2*inj_params_bbh['iota'])*hgrfp1
hvf2=Av*np.sqrt(525./56.)*np.sin(2*inj_params_bbh['iota'])*hgrfp2
hvf3=Av*np.sqrt(525./56.)*np.sin(2*inj_params_bbh['iota'])*hgrfp3

hgrfp1, hgrfc1, hgrfp2, hgrfc2, hgrfp3, hgrfc3 = None, None, None, None, None, None
hcross1, hplus1, hcross2, hplus2, hcross3, hplus3 = None, None, None, None, None, None
# In[41]:

ffff1=np.array([np.zeros_like(fre1_td1, dtype=np.cfloat)]*4, dtype=object)
ffff2=np.array([np.zeros_like(fre1_td2, dtype=np.cfloat)]*5, dtype=object)
ffff3=np.array([np.zeros_like(fre1_td3, dtype=np.cfloat)]*5, dtype=object)
def onecont1(conteggio):
    #print(j)
    sys.stdout.flush()
    if conteggio <= 3:
        affff1=np.pad(data1[conteggio], (int(f_lo1/df1),int((f_hi1-f_hi)/df1)))

    affff2=np.pad(data2[conteggio], (int(f_lo2/df2),int((f_hi1-f_hi)/df2)))
    affff3=np.pad(data3[conteggio], (int(f_lo3/df3),int((f_hi1-f_hi)/df3)))

    if conteggio <= 3:
        loc1=str(network_spec1[conteggio]).split('_')
        loc1=loc1[1]
    loc2=str(network_spec2[conteggio]).split('_')
    loc2=loc2[1]
    loc3=str(network_spec3[conteggio]).split('_')
    loc3=loc3[1]

    if conteggio <= 3:
        pf1, cf1, vf1, wf1, bf1, lf1 = apkons(inj_params_bbh['ra'],inj_params_bbh['dec'],f1,inj_params_bbh['Mc'],inj_params_bbh['tc'],inj_params_bbh['psi'],inj_params_bbh['gmst0'],loc1,use_rot, time=False)
    pf2, cf2, vf2, wf2, bf2, lf2 = apkons(inj_params_bbh['ra'],inj_params_bbh['dec'],f2,inj_params_bbh['Mc'],inj_params_bbh['tc'],inj_params_bbh['psi'],inj_params_bbh['gmst0'],loc2,use_rot, time=False)
    pf3, cf3, vf3, wf3, bf3, lf3 = apkons(inj_params_bbh['ra'],inj_params_bbh['dec'],f3,inj_params_bbh['Mc'],inj_params_bbh['tc'],inj_params_bbh['psi'],inj_params_bbh['gmst0'],loc3,use_rot, time=False)

    if use_rot:

        if conteggio <= 3:
            pf1=np.pad(pf1, ((int(f_lo1/df1), int((f_hi1-f_hi)/df1))))
        pf2=np.pad(pf2, ((int(f_lo2/df2), int((f_hi1-f_hi)/df2))))
        pf3=np.pad(pf3, ((int(f_lo3/df3), int((f_hi1-f_hi)/df3))))
        if conteggio <= 3:
            cf1=np.pad(cf1, ((int(f_lo1/df1), int((f_hi1-f_hi)/df1))))
        cf2=np.pad(cf2, ((int(f_lo2/df2), int((f_hi1-f_hi)/df2))))
        cf3=np.pad(cf3, ((int(f_lo3/df3), int((f_hi1-f_hi)/df3))))
        if conteggio <= 3:
            vf1=np.pad(vf1, ((int(f_lo1/df1), int((f_hi1-f_hi)/df1))))
        vf2=np.pad(vf2, ((int(f_lo2/df2), int((f_hi1-f_hi)/df2))))
        vf3=np.pad(vf3, ((int(f_lo3/df3), int((f_hi1-f_hi)/df3))))
        if conteggio <= 3:
            wf1=np.pad(wf1, ((int(f_lo1/df1), int((f_hi1-f_hi)/df1))))
        wf2=np.pad(wf2, ((int(f_lo2/df2), int((f_hi1-f_hi)/df2))))
        wf3=np.pad(wf3, ((int(f_lo3/df3), int((f_hi1-f_hi)/df3))))
        if conteggio <= 3:
            bf1=np.pad(bf1, ((int(f_lo1/df1), int((f_hi1-f_hi)/df1))))
        bf2=np.pad(bf2, ((int(f_lo2/df2), int((f_hi1-f_hi)/df2))))
        bf3=np.pad(bf3, ((int(f_lo3/df3), int((f_hi1-f_hi)/df3))))
        if conteggio <= 3:
            lf1=np.pad(lf1, ((int(f_lo1/df1), int((f_hi1-f_hi)/df1))))
        lf2=np.pad(lf2, ((int(f_lo2/df2), int((f_hi1-f_hi)/df2))))
        lf3=np.pad(lf3, ((int(f_lo3/df3), int((f_hi1-f_hi)/df3))))

    if bool_scal:
        if conteggio <= 3:
            affff1 += bf1*hscalf1
        affff2 += bf2*hscalf2
        affff3 += bf3*hscalf3

    if bool_vec:
        if conteggio <= 3:
            affff1 += (vf1*hvf1 + wf1*hwf1)
        affff2 += (vf2*hvf2 + wf2*hwf2)
        affff3 += (vf3*hvf3 + wf3*hwf3)
    if conteggio <= 3:
        np.save(''.join(('datafiles/', str(Mc_bbhf),'_s1_',str(distanza_l), '_HLVK_', str(inj_params_bbh['eta']),'_' , 'ffff_PARALLEL_', str(conteggio))),affff1)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s1_',str(distanza_l), '_HLVKI_', str(inj_params_bbh['eta']),'_' , 'ffff_PARALLEL_', str(conteggio))),affff2)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s1_',str(distanza_l), '_ETCE_', str(inj_params_bbh['eta']),'_' , 'ffff_PARALLEL_', str(conteggio))),affff3)

    
if __name__ == '__main__':
    start1 = timer()
    processes = []
    for j in range(0,5):
        p2= multiprocessing.Process(target=onecont1, args=(j,))
        processes.append(p2)
        p2.start()
    for process in processes:
        process.join()
    for j in range(0,5):
        if j <= 3:
            ffff1[j,] = np.load(file = ''.join(('datafiles/', str(Mc_bbhf),'_s1_',str(distanza_l), '_HLVK_', str(inj_params_bbh['eta']),'_' , 'ffff_PARALLEL_', str(j), '.npy')))
        ffff2[j,] = np.load(file = ''.join(('datafiles/', str(Mc_bbhf),'_s1_',str(distanza_l), '_HLVKI_', str(inj_params_bbh['eta']),'_' , 'ffff_PARALLEL_', str(j), '.npy')))
        ffff3[j,] = np.load(file = ''.join(('datafiles/', str(Mc_bbhf),'_s1_',str(distanza_l), '_ETCE_', str(inj_params_bbh['eta']),'_' , 'ffff_PARALLEL_', str(j), '.npy')))

        if j <= 3:
            os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s1_',str(distanza_l), '_HLVK_', str(inj_params_bbh['eta']),'_' , 'ffff_PARALLEL_', str(j), '.npy')))
        os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s1_',str(distanza_l), '_HLVKI_', str(inj_params_bbh['eta']),'_' , 'ffff_PARALLEL_', str(j), '.npy')))
        os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s1_',str(distanza_l), '_ETCE_', str(inj_params_bbh['eta']),'_' , 'ffff_PARALLEL_', str(j), '.npy')))
        
noise_asd1=np.zeros_like(f1)
noise_asd2=np.zeros_like(f2)
noise_asd3=np.zeros_like(f3)
data1, data2, data3 = None, None, None
p1, p2, p3 = None, None, None
c1, c2, c3 = None, None, None
hvf1, hvf2, hvf3 =  None, None, None
hwf1, hwf2, hwf3 =  None, None, None
hscalf1, hscalf2, hscalf3 =  None, None, None

for i in range (0, len(f1)):
    for j in range (1, len(noise1[0])):
        if float(f1[i]) <= float(noise1[0,j]) and float(f1[i])>= float(noise1[0,j-1]):
            noise_asd1[i]=(noise1[1,j]-noise1[1,j-1] )/(noise1[0,j]-noise1[0,j-1] )*(f1[i]-noise1[0,j-1]) + noise1[1,j-1]
for i in range (0, len(f2)):
    for j in range (1, len(noise2[0])):
        if float(f2[i]) <= float(noise2[0,j]) and float(f2[i])>= float(noise2[0,j-1]):
            noise_asd2[i]=(noise2[1,j]-noise2[1,j-1] )/(noise2[0,j]-noise2[0,j-1] )*(f2[i]-noise2[0,j-1]) + noise2[1,j-1]
for i in range (0, len(f3)):
    for j in range (1, len(noise3[0])):
        if float(f3[i]) <= float(noise3[0,j]) and float(f3[i])>= float(noise3[0,j-1]):
            noise_asd3[i]=(noise3[1,j]-noise3[1,j-1] )/(noise3[0,j]-noise3[0,j-1] )*(f3[i]-noise3[0,j-1]) + noise3[1,j-1]

noise1, noise2, noise3 = None, None, None


# In[42]:


df1_prova, df2_prova, df3_prova, =2**-4, 2**-4, 2**-4
f_prova1=np.arange(f_lo1, f_hi, df1_prova)
f_prova2=np.arange(f_lo2, f_hi, df2_prova)
f_prova3=np.arange(f_lo3, f_hi, df3_prova)

t_prova1 = np.linspace(-tc1, 0, 2*int(f_hi1/df1_prova)-1)
t_prova2 = np.linspace(-tc2, 0, 2*int(f_hi1/df2_prova)-1)
t_prova3 = np.linspace(-tc3, 0, 2*int(f_hi1/df3_prova)-1)
net_tdp1 = network.Network(network_spec1)
net_tdp2 = network.Network(network_spec2)
net_tdp3 = network.Network(network_spec3)


net_tdp1.set_wf_vars(wf_model_name='tf2')
net_tdp1.set_net_vars(f=f_prova1, inj_params=inj_params_bbh, deriv_symbs_string=deriv_symbs_string,
                    conv_cos=conv_cos, conv_log=conv_log, use_rot=use_rot)
net_tdp1.calc_wf_polarizations()
net_tdp1.calc_wf_polarizations_derivs_num()
net_tdp1.setup_ant_pat_lpf_psds()
net_tdp1.calc_det_responses()
net_tdp1.calc_det_responses_derivs_num()
net_tdp1.calc_snrs()
net_tdp1.calc_errors(cond_sup=np.inf)
net_tdp1.calc_sky_area_90()
sys.stdout.flush()

net_tdp2.set_wf_vars(wf_model_name='tf2')
net_tdp2.set_net_vars(f=f_prova2, inj_params=inj_params_bbh, deriv_symbs_string=deriv_symbs_string,
                    conv_cos=conv_cos, conv_log=conv_log, use_rot=use_rot)
net_tdp2.calc_wf_polarizations()
net_tdp2.calc_wf_polarizations_derivs_num()
net_tdp2.setup_ant_pat_lpf_psds()
net_tdp2.calc_det_responses()
net_tdp2.calc_det_responses_derivs_num()
net_tdp2.calc_snrs()
net_tdp2.calc_errors(cond_sup=np.inf)
net_tdp2.calc_sky_area_90()
sys.stdout.flush()

net_tdp3.set_wf_vars(wf_model_name='tf2')
net_tdp3.set_net_vars(f=f_prova3, inj_params=inj_params_bbh, deriv_symbs_string=deriv_symbs_string,
                    conv_cos=conv_cos, conv_log=conv_log, use_rot=use_rot)
net_tdp3.calc_wf_polarizations()
net_tdp3.calc_wf_polarizations_derivs_num()
net_tdp3.setup_ant_pat_lpf_psds()
net_tdp3.calc_det_responses()
net_tdp3.calc_det_responses_derivs_num()
net_tdp3.calc_snrs()
net_tdp3.calc_errors(cond_sup=np.inf)
net_tdp3.calc_sky_area_90()
sys.stdout.flush()
ntot=100
grid1=np.random.multivariate_normal([np.pi/4, np.pi/4.0], [[net_tdp1.cov[8,8], net_tdp1.cov[8,9]], [net_tdp1.cov[9,8], net_tdp1.cov[9,9]]], size=(ntot) )
grid2=np.random.multivariate_normal([np.pi/4, np.pi/4.0], [[net_tdp2.cov[8,8], net_tdp2.cov[8,9]], [net_tdp2.cov[9,8], net_tdp2.cov[9,9]]], size=(ntot) )
grid3=np.random.multivariate_normal([np.pi/4, np.pi/4.0], [[net_tdp3.cov[8,8], net_tdp3.cov[8,9]], [net_tdp3.cov[9,8], net_tdp3.cov[9,9]]], size=(ntot) )
ras1, decs1 = grid1[:,0] , grid1[:,1]
ras2, decs2 = grid2[:,0] , grid2[:,1]
ras3, decs3 = grid3[:,0] , grid3[:,1]

ra1=np.sqrt(net_tdp1.cov[8,8])
dec1=np.sqrt(net_tdp1.cov[9,9])

ra2=np.sqrt(net_tdp2.cov[8,8])
dec2=np.sqrt(net_tdp2.cov[9,9])

ra3=np.sqrt(net_tdp3.cov[8,8])
dec3=np.sqrt(net_tdp3.cov[9,9])

setup_plot((10,6), 'RA', 'DEC')
hist5, xbins5, ybins5 = np.histogram2d(ras1,decs1, bins=20, range=[[np.pi/4-10*ra1, np.pi/4+10*ra1],[np.pi/4-10*dec1, np.pi/4+10*dec1]])
extent=[np.pi/4-10*ra1, np.pi/4+10*ra1,np.pi/4-10*dec1, np.pi/4+10*dec1]
plt.imshow(hist5.T, extent=extent , origin='lower')
plt.savefig(''.join(('histograms/hist_NPSD2_HLVK_s1_' , str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
print(np.sqrt(net_tdp1.cov[8,8]), np.sqrt(net_tdp1.cov[9,9]))

err_m1=net_tdp1.errs['Mc']
err_eta1=net_tdp1.errs['eta']
err_dl1=net_tdp1.errs['DL']

err_m2=net_tdp2.errs['Mc']
err_eta2=net_tdp2.errs['eta']
err_dl2=net_tdp2.errs['DL']

err_m3=net_tdp3.errs['Mc']
err_eta3=net_tdp3.errs['eta']
err_dl3=net_tdp3.errs['DL']


area1=net_tdp1.errs['sky_area_90']
area2=net_tdp2.errs['sky_area_90']
area3=net_tdp3.errs['sky_area_90']

setup_plot((10,6), 'RA', 'DEC')
hist5, xbins5, ybins5 = np.histogram2d(ras2,decs2, bins=20, range=[[np.pi/4-10*ra2, np.pi/4+10*ra2],[np.pi/4-10*dec2, np.pi/4+10*dec2]])
extent=[np.pi/4-10*ra2, np.pi/4+10*ra2,np.pi/4-10*dec2, np.pi/4+10*dec2]
plt.imshow(hist5.T, extent=extent , origin='lower')
plt.savefig(''.join(('histograms/hist_NPSD2_HLVKI_s1_' , str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
print(np.sqrt(net_tdp2.cov[8,8]), np.sqrt(net_tdp2.cov[9,9]))
sys.stdout.flush()

setup_plot((10,6), 'RA', 'DEC')
hist5, xbins5, ybins5 = np.histogram2d(ras3,decs3, bins=20, range=[[np.pi/4-10*ra3, np.pi/4+10*ra3],[np.pi/4-10*dec3, np.pi/4+10*dec3]])
extent=[np.pi/4-10*ra3, np.pi/4+10*ra3,np.pi/4-10*dec3, np.pi/4+10*dec3]
plt.imshow(hist5.T, extent=extent , origin='lower')
plt.savefig(''.join(('histograms/hist_NPSD2_ETCE_s1_' , str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
print(np.sqrt(net_tdp3.cov[8,8]), np.sqrt(net_tdp3.cov[9,9]))
sys.stdout.flush()

grid1, grid2, grid3 = None, None, None
net_tdp1, net_tdp2, net_tdp3= None, None, None 
f_prova1, f_prova2, f_prova3= None, None, None
t_prova1, t_prova2, t_prova3= None, None, None


# In[43]:


cos = np.cos
sin = np.sin
exp = np.exp
PI = np.pi


from gwbench.basic_constants import time_fac, REarth, AU, cLight

def rrot_mat(angle,axis):
    c = np.cos(angle)
    s = np.sin(angle)

    if axis == 'i':
        return np.array( ( (1,0,0), (0,c,-s), (0,s,c) ) )
    if axis == 'j':
        return np.array( ( (c,0,s), (0,1,0), (-s,0,c) ) )
    if axis == 'k':
        return np.array( ( (c,-s,0), (s,c,0), (0,0,1) ) )

def ddet_angles(loc):
    # return alpha, beta, gamma in radians
    # alpha ... longitude
    # beta  ... pi/2 - latitude
    # gamma ... angle from 'Due East' to y-arm
    if loc == 'H':
        return -2.08406, PI/2.-0.810795, PI-5.65488
    elif loc == 'L':
        return -1.58431, PI/2.-0.533423, PI-4.40318
    elif loc in ('V','ET1','ET2','ET3'):
        return 0.183338, PI/2.-0.761512, PI-0.33916
    elif loc == 'K':
        return 2.3942, PI/2.-0.632682, PI-1.054113
    elif loc == 'I':
        return 1.334013, PI/2.-0.248418, PI-1.570796

    #elif loc == 'C':
    #    return -1.969174, PI/2.-0.764918, 0.
    #elif loc == 'N':
    #    return -1.8584265, PI/2.-0.578751, -PI/3.
    #elif loc == 'S':
    #    return 2.530727, PI/2.+0.593412, PI/4.
    elif loc == 'C':
        return -1.969174, PI/2.-0.764918, 0.
    elif loc == 'N':
        return -2.1817, PI/2.-0.8029, 3*PI/4.
    elif loc == 'S':
        return -1.64061, PI/2.-0.50615, 0.

def ddet_ten_and_loc_vec(loc, R):
    i_vec = np.array((1,0,0))
    j_vec = np.array((0,1,0))
    k_vec = np.array((0,0,1))

    et_vec2 = ( i_vec + np.sqrt(3.)*j_vec)/2.
    et_vec3 = (-i_vec + np.sqrt(3.)*j_vec)/2.

    alpha, beta, gamma = ddet_angles(loc)
    EulerD1 = np.matmul(np.matmul(rrot_mat(alpha,'k'), rrot_mat(beta,'j')), rrot_mat(gamma,'k'))

    if loc in   ('ET3','LISA3'):
        eDArm1 = -1 * np.matmul(EulerD1,et_vec2)
        eDArm2 = -1 * np.matmul(EulerD1,et_vec3)
    elif loc in ('ET2','LISA2'):
        eDArm1 =      np.matmul(EulerD1,et_vec3)
        eDArm2 = -1 * np.matmul(EulerD1,i_vec)
    elif loc in ('ET1','LISA1'):
        eDArm1 =      np.matmul(EulerD1,i_vec)
        eDArm2 =      np.matmul(EulerD1,et_vec2)
    else:
        eDArm1 = np.matmul(EulerD1,i_vec)
        eDArm2 = np.matmul(EulerD1,j_vec)

    return np.outer(eDArm1,eDArm1) - np.outer(eDArm2,eDArm2), R/cLight * np.matmul(EulerD1,k_vec)

def ap_nontens(ra,dec,fr,Mc,tc,psi,gmst0,loc,use_rot=False):
    half_period = 4.32e4
    R = REarth

    D, d = ddet_ten_and_loc_vec(loc, R)

    if use_rot:
        tf = tc - (5./256.)*(time_fac*Mc)**(-5./3.)*(PI*fr)**(-8./3.)
    else:
        tf = 0

    gra = (gmst0 + tf*PI/half_period) - ra
    theta = PI/2. - dec

    if isinstance(gra, np.ndarray):
        r = np.array((cos(gra) * sin(theta), sin(gra) * sin(theta), cos(theta) * np.ones(len(gra))))
        XX = np.transpose(np.array([ -cos(psi)*sin(gra) - sin(psi)*cos(gra)*sin(dec), -cos(psi)*cos(gra) + sin(psi)*sin(gra)*sin(dec), sin(psi)*cos(dec) * np.ones(len(gra)) ]))
        YY = np.transpose(np.array([  sin(psi)*sin(gra) - cos(psi)*cos(gra)*sin(dec),  sin(psi)*cos(gra) + cos(psi)*sin(gra)*sin(dec), cos(psi)*cos(dec) * np.ones(len(gra)) ]))
        ZZ = np.transpose(np.array([  -cos(gra)*cos(dec) , cos(dec)*sin(gra) , -sin(dec)* np.ones(len(gra))]))
        Fp = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),XX[i]) - np.matmul(np.matmul(YY[i],D),YY[i]) for i in range (len(gra))])
        Fc = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),YY[i]) + np.matmul(np.matmul(YY[i],D),XX[i]) for i in range (len(gra))])
        Fs1 = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),XX[i]) + np.matmul(np.matmul(YY[i],D),YY[i]) for i in range(len(gra))])
        Fs2 = 0.5 * np.array([np.matmul(np.matmul(ZZ[i],D),ZZ[i]) for i in range(len(gra))])
        Fv = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),ZZ[i]) + np.matmul(np.matmul(ZZ[i],D),XX[i]) for i in range(len(gra))])
        Fw = 0.5 * np.array([np.matmul(np.matmul(YY[i],D),ZZ[i]) + np.matmul(np.matmul(ZZ[i],D),YY[i]) for i in range(len(gra))])
    else:
        r = np.array((cos(gra) * sin(theta), sin(gra) * sin(theta), cos(theta)))
        XX = np.transpose(np.array([ -cos(psi)*sin(gra) - sin(psi)*cos(gra)*sin(dec), -cos(psi)*cos(gra) + sin(psi)*sin(gra)*sin(dec), sin(psi)*cos(dec) ]))
        YY = np.transpose(np.array([  sin(psi)*sin(gra) - cos(psi)*cos(gra)*sin(dec),  sin(psi)*cos(gra) + cos(psi)*sin(gra)*sin(dec), cos(psi)*cos(dec) ]))
        ZZ = np.transpose(np.array([  -cos(gra)*cos(dec) , cos(dec)*sin(gra) , -sin(dec)]))
        Fp = 0.5 * (np.matmul(np.matmul(XX,D),XX) - np.matmul(np.matmul(YY,D),YY))
        Fc = 0.5 * (np.matmul(np.matmul(XX,D),YY) + np.matmul(np.matmul(YY,D),XX))
        Fs1 = 0.5 * (np.matmul(np.matmul(XX,D),XX) + np.matmul(np.matmul(YY,D),YY))
        Fs2 = 0.5 * (np.matmul(np.matmul(ZZ,D),ZZ))
        Fv = 0.5 * (np.matmul(np.matmul(XX,D),ZZ) + np.matmul(np.matmul(ZZ,D),XX))
        Fw = 0.5 * (np.matmul(np.matmul(YY,D),ZZ) + np.matmul(np.matmul(ZZ,D),YY))
        
    return np.array([exp(1j * 2*PI * fr * np.matmul(d,r)) , Fp, Fc,  Fs2 , Fv , Fw])

def calc_ap(nett, ra, dec, freq,freq_max, Mc, tc, psi, gmst0, use_rot):
    df = freq[1]-freq[0]
    if freq_max.all != None:
        net_aps=[[np.zeros_like(freq_max, dtype=np.cfloat)]*5]*len(nett.detectors)
        
        net_aps=np.cfloat(net_aps)
        for det in nett.detectors:
            loc=det.loc
            net_aps[nett.detectors.index(det),0:5, int((freq[0]-freq_max[0])/df):int((abs(freq_max[0]-freq[0])/df))+
                                                    len(freq)] = ap_nontens(ra,dec,freq,Mc,tc,psi,gmst0,loc,use_rot)[1:6,:]
            net_aps[nett.detectors.index(det)] = np.array(net_aps[nett.detectors.index(det)], dtype=object)

    else:
        '''
        
        THIS NEEDS DEBUGGING!!!
        
        
        '''
        net_aps=[[np.zeros_like(freq)]*5]*len(nett.detectors)
        for det in nett.detectors:
            #print(det)
            loc=det.loc
            net_aps[nett.detectors.index(det)][0:5][:] = ap_nontens(ra,dec,freq,Mc,tc,psi,gmst0,loc,use_rot)[1:6][:]
            #print(np.nonzero(net_aps[nett.detectors.index(det)][0]).shape)
            net_aps[nett.detectors.index(det)] = np.array(net_aps[nett.detectors.index(det)], dtype=object)
            
    net_aps = np.array(net_aps, dtype=object)
    net_aps[nett.detectors.index(det),:]= np.array(net_aps[nett.detectors.index(det),:], dtype=object)
    return net_aps


# In[44]:


s1, s2, s3 = [], [], []
v1, v2, v3 = [], [], []
w1, w2, w3 = [], [], []
n3d1, n3d2, n3d3 = [], [], []
net_td1 = network.Network(network_spec1)
net_td2 = network.Network(network_spec2)
net_td3 = network.Network(network_spec3)
wf_model_name = 'tf2'
net_td1.set_wf_vars(wf_model_name=wf_model_name)
net_td2.set_wf_vars(wf_model_name=wf_model_name)
net_td3.set_wf_vars(wf_model_name=wf_model_name)


eta=etazonz
def megaf(conteggio):
    p=1
    global null1
    global null2
    global null3
    global ffff1
    global ffff2
    global ffff3
    print(conteggio)
    sys.stdout.flush()
    use_rot = True

    inj_params_td1                   = dict()

    inj_params_td1['Mc']              = Mc_bbh
    inj_params_td1['eta']             = eta
    inj_params_td1['chi1z']           = 0.0
    inj_params_td1['chi2z']           = 0.0
    inj_params_td1['DL']              = distanza_l
    inj_params_td1['tc']              = 0.0
    inj_params_td1['phic']            = 0.0
    inj_params_td1['iota']            = np.pi/4
    inj_params_td1['ra']              = ras1[conteggio]
    inj_params_td1['dec']             = decs1[conteggio]
    inj_params_td1['psi']             = np.pi/4
    inj_params_td1['gmst0']           = 0.0

    inj_params_td3                    = dict()

    inj_params_td3['Mc']              = Mc_bbh
    inj_params_td3['eta']             = eta
    inj_params_td3['chi1z']           = 0.0
    inj_params_td3['chi2z']           = 0.0
    inj_params_td3['DL']              = distanza_l
    inj_params_td3['tc']              = 0.0
    inj_params_td3['phic']            = 0.0
    inj_params_td3['iota']            = np.pi/4
    inj_params_td3['ra']              = ras3[conteggio]
    inj_params_td3['dec']             = decs3[conteggio]
    inj_params_td3['psi']             = np.pi/4
    inj_params_td3['gmst0']           = 0.0

    inj_params_td2                   = dict()

    inj_params_td2['Mc']              = Mc_bbh
    inj_params_td2['eta']             = eta
    inj_params_td2['chi1z']           = 0.0
    inj_params_td2['chi2z']           = 0.0
    inj_params_td2['DL']              = distanza_l
    inj_params_td2['tc']              = 0.0
    inj_params_td2['phic']            = 0.0
    inj_params_td2['iota']            = np.pi/4
    inj_params_td2['ra']              = ras2[conteggio]
    inj_params_td2['dec']             = decs2[conteggio]
    inj_params_td2['psi']             = np.pi/4
    inj_params_td2['gmst0']           = 0.0

    deriv_symbs_string = 'Mc eta DL tc phic iota ra dec psi'
    conv_cos = ('')
    conv_log = ('')

    fre1= np.pad(f1, (int(f_lo1/df1), int((f_hi1-f_hi)/df1)))
    fre2= np.pad(f2, (int(f_lo2/df2), int((f_hi1-f_hi)/df2)))
    fre3= np.pad(f3, (int(f_lo3/df3), int((f_hi1-f_hi)/df3)))
    pf1=np.array(np.cfloat([np.zeros_like(fre1)]*4), dtype=object)
    pf2=np.array(np.cfloat([np.zeros_like(fre2)]*5), dtype=object)
    pf3=np.array(np.cfloat([np.zeros_like(fre3)]*5), dtype=object)
    cf1=np.array(np.cfloat([np.zeros_like(fre1)]*4), dtype=object)
    cf2=np.array(np.cfloat([np.zeros_like(fre2)]*5), dtype=object)
    cf3=np.array(np.cfloat([np.zeros_like(fre3)]*5), dtype=object)
    vf1=np.array(np.cfloat([np.zeros_like(fre1)]*4), dtype=object)
    vf2=np.array(np.cfloat([np.zeros_like(fre2)]*5), dtype=object)
    vf3=np.array(np.cfloat([np.zeros_like(fre3)]*5), dtype=object)
    wf1=np.array(np.cfloat([np.zeros_like(fre1)]*4), dtype=object)
    wf2=np.array(np.cfloat([np.zeros_like(fre2)]*5), dtype=object)
    wf3=np.array(np.cfloat([np.zeros_like(fre3)]*5), dtype=object)
    bf1=np.array(np.cfloat([np.zeros_like(fre1)]*4), dtype=object)
    bf2=np.array(np.cfloat([np.zeros_like(fre2)]*5), dtype=object)
    bf3=np.array(np.cfloat([np.zeros_like(fre3)]*5), dtype=object)
    lf1=np.array(np.cfloat([np.zeros_like(fre1)]*4), dtype=object)
    lf2=np.array(np.cfloat([np.zeros_like(fre2)]*5), dtype=object)
    lf3=np.array(np.cfloat([np.zeros_like(fre3)]*5), dtype=object)
    
    Fnet1= calc_ap(net_td1, ras1[conteggio], decs1[conteggio], f1, fre1, 35, 0, np.pi/4, 0, True)
    Fnet2= calc_ap(net_td2, ras2[conteggio], decs2[conteggio], f2, fre2, 35, 0, np.pi/4, 0, True)
    Fnet3= calc_ap(net_td3, ras3[conteggio], decs3[conteggio], f3, fre3, 35, 0, np.pi/4, 0, True)

    
    for i in range (0, 5):

        loc2=str(network_spec2[i]).split('_')
        loc2=loc2[1]
        if i < 4:
            pf1[i], cf1[i], vf1[i], wf1[i], bf1[i]= Fnet1[i,0], Fnet1[i,1], Fnet1[i,3], Fnet1[i,4], Fnet1[i,2]
        pf2[i], cf2[i], vf2[i], wf2[i], bf2[i]= Fnet2[i,0], Fnet2[i,1], Fnet2[i,3], Fnet2[i,4], Fnet2[i,2]
        pf3[i], cf3[i], vf3[i], wf3[i], bf3[i]= Fnet3[i,0], Fnet3[i,1], Fnet3[i,3], Fnet3[i,4], Fnet3[i,2]
    null1=np.zeros((len(fre1_td1)), dtype=np.cfloat)
    null2=np.zeros((len(fre1_td2)), dtype=np.cfloat)
    null3=np.zeros((len(fre1_td3)), dtype=np.cfloat)
    if use_rot:
        
        null1[:]=(pf1[2,:]*cf1[3,:] - pf1[3,:]*cf1[2,:])*ffff1[1,:] + (pf1[3,:]*cf1[1,:] 
            - pf1[1,:]*cf1[3,:])*ffff1[2,:] + (pf1[1,:]*cf1[2,:] - pf1[2,:]*cf1[1,:])*ffff1[3,:]
        
        null2[:]=(pf2[3,:]*cf2[4,:] - pf2[4,:]*cf2[3,:])*ffff2[2,:] + (pf2[4,:]*cf2[2,:] 
            - pf2[2,:]*cf2[4,:])*ffff2[3,:] + (pf2[2,:]*cf2[3,:] - pf2[3,:]*cf2[2,:])*ffff2[4,:]
        
        null3[:]=(pf3[2,:]*cf3[3,:] - pf3[3,:]*cf3[2,:])*ffff3[1,:] + (pf3[3,:]*cf3[1,:] 
            - pf3[1,:]*cf3[3,:])*ffff3[2,:] + (pf3[1,:]*cf3[2,:] - pf3[2,:]*cf3[1,:])*ffff3[3,:]
    
    
    
    S1=null1*np.conjugate(null1)#*df_bbh*2
    null1=None
    S2=null2*np.conjugate(null2)#*df_bbh*2
    null2=None
    S3=null3*np.conjugate(null3)#*df_bbh*2
    null3=None
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVK_', 
                     str(inj_params_td1['eta']),'_' , '3S1_', str(conteggio))), S1[int(f_lo1/df1):int(f_hi/df1)+1].real)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVKI_',
                     str(inj_params_td1['eta']),'_' , '3S2_', str(conteggio))), S2[int(f_lo2/df2):int(f_hi/df2)+1].real)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_ETCE_',
                     str(inj_params_td1['eta']),'_' , '3S3_', str(conteggio))), S3[int(f_lo3/df3):int(f_hi/df3)+1].real)
    if use_rot:
        d1=np.zeros((3, len(fre1), 4, 4), dtype=np.cfloat)
        d2=np.zeros((3, len(fre2), 4, 4), dtype=np.cfloat)
        d3=np.zeros((3, len(fre3), 4, 4), dtype=np.cfloat)
        
        for j in range (0,4):
            sa1=np.transpose(np.array([ffff1[j,], bf1[j,], pf1[j,], cf1[j,]]))
            va1=np.transpose(np.array([ffff1[j,], vf1[j,], pf1[j,], cf1[j,]]))
            wa1=np.transpose(np.array([ffff1[j,], wf1[j,], pf1[j,], cf1[j,]]))
            
            
            sa2=np.transpose(np.array([ffff2[j+1,], bf2[j+1,], pf2[j+1,], cf2[j+1,]]))
            va2=np.transpose(np.array([ffff2[j+1,], vf2[j+1,], pf2[j+1,], cf2[j+1,]]))
            wa2=np.transpose(np.array([ffff2[j+1,], wf2[j+1,], pf2[j+1,], cf2[j+1,]]))

            sa3=np.transpose(np.array([ffff3[j+1,], bf3[j+1,], pf3[j+1,], cf3[j+1,]]))
            va3=np.transpose(np.array([ffff3[j+1,], vf3[j+1,], pf3[j+1,], cf3[j+1,]]))
            wa3=np.transpose(np.array([ffff3[j+1,], wf3[j+1,], pf3[j+1,], cf3[j+1,]]))
            d1[0,:,j,:] = sa1[:,:]
            d2[0,:,j,:] = sa2[:,:]
            d3[0,:,j,:] = sa3[:,:]
            d1[1,:,j,:] = va1[:,:]
            d2[1,:,j,:] = va2[:,:]
            d3[1,:,j,:] = va3[:,:]
            d1[2,:,j,:] = wa1[:,:]
            d2[2,:,j,:] = wa2[:,:]
            d3[2,:,j,:] = wa3[:,:]
        ffff1, ffff2, ffff3 = None, None, None
        pf1, pf2, pf3 = None, None, None
        cf1, cf2, cf3 = None, None, None
        vf1, vf2, vf3 = None, None, None
        wf1, wf2, wf3 = None, None, None
        bf1, bf2, bf3 = None, None, None
        det1=np.linalg.det(d1)
        d1=None
        det2=np.linalg.det(d2)
        d2=None
        det3=np.linalg.det(d3)
        d3=None
        psds1= det1[0,]*np.conjugate(det1[0,])
        psds2= det2[0,]*np.conjugate(det2[0,])
        psds3= det3[0,]*np.conjugate(det3[0,])
        
        psdv1= det1[1,]*np.conjugate(det1[1,])
        psdv2= det2[1,]*np.conjugate(det2[1,])
        psdv3= det3[1,]*np.conjugate(det3[1,])
        
        psdw1= det1[2,]*np.conjugate(det1[2,])
        psdw2= det2[2,]*np.conjugate(det2[2,])
        psdw3= det3[2,]*np.conjugate(det3[2,])
        det1, det2, det3 = None, None, None
        
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVK_', str(inj_params_td1['eta']),'_' , 's1_', str(conteggio))), psds1[np.nonzero(psds1)].real)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVKI_', str(inj_params_td2['eta']),'_' , 's2_', str(conteggio))), psds2[np.nonzero(psds2)].real)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_ETCE_', str(inj_params_td3['eta']),'_' , 's3_', str(conteggio))), psds3[np.nonzero(psds3)].real)
    
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVK_', str(inj_params_td1['eta']),'_' , 'v1_', str(conteggio))), psdv1[np.nonzero(psdv1)].real)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVKI_', str(inj_params_td2['eta']),'_' , 'v2_', str(conteggio))), psdv2[np.nonzero(psdv2)].real)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_ETCE_', str(inj_params_td3['eta']),'_' , 'v3_', str(conteggio))), psdv3[np.nonzero(psdv3)].real)
    
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVK_', str(inj_params_td1['eta']),'_' , 'w1_', str(conteggio))), psdw1[np.nonzero(psdw1)].real)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVKI_', str(inj_params_td2['eta']),'_' , 'w2_', str(conteggio))), psdw2[np.nonzero(psdw2)].real)
    np.save(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_ETCE_', str(inj_params_td3['eta']),'_' , 'w3_', str(conteggio))), psdw3[np.nonzero(psdw3)].real)


# In[45]:


if __name__ == '__main__':
    starttime = time.time()
    processes = []
    for j in range(0,ntot):
        p = multiprocessing.Process(target=megaf, args=(j,))
        processes.append(p)
        p.start()

    for process in processes:
        process.join()


# In[46]:


for j in range (0,ntot):
    n3d1.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVK_', 
                     str(inj_params_bbh['eta']),'_' , '3S1_', str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVK_', 
                     str(inj_params_bbh['eta']),'_' , '3S1_', str(j), '.npy')))
    n3d2.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVKI_', 
                     str(inj_params_bbh['eta']),'_' , '3S2_', str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_HLVKI_', 
                     str(inj_params_bbh['eta']),'_' , '3S2_', str(j), '.npy')))
    n3d3.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_ETCE_', 
                     str(inj_params_bbh['eta']),'_' , '3S3_', str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l), '_ETCE_', 
                     str(inj_params_bbh['eta']),'_' , '3S3_', str(j), '.npy')))
    
    s1.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVK_', str(inj_params_bbh['eta']),'_' , 's1_' , str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVK_', str(inj_params_bbh['eta']),'_' , 's1_' , str(j), '.npy')))
    s2.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVKI_', str(inj_params_bbh['eta']),'_' , 's2_' , str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVKI_', str(inj_params_bbh['eta']),'_' , 's2_' , str(j), '.npy')))
    s3.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_ETCE_', str(inj_params_bbh['eta']),'_' , 's3_' , str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_ETCE_', str(inj_params_bbh['eta']),'_' , 's3_' , str(j), '.npy')))
    
    v1.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVK_', str(inj_params_bbh['eta']),'_' , 'v1_' , str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVK_', str(inj_params_bbh['eta']),'_' , 'v1_' , str(j), '.npy')))
    v2.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVKI_', str(inj_params_bbh['eta']),'_' , 'v2_' , str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVKI_', str(inj_params_bbh['eta']),'_' , 'v2_' , str(j), '.npy')))
    v3.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_ETCE_', str(inj_params_bbh['eta']),'_' , 'v3_' , str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_ETCE_', str(inj_params_bbh['eta']),'_' , 'v3_' , str(j), '.npy')))
    
    w1.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVK_', str(inj_params_bbh['eta']),'_' , 'w1_' , str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVK_', str(inj_params_bbh['eta']),'_' , 'w1_' , str(j), '.npy')))
    w2.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVKI_', str(inj_params_bbh['eta']),'_' , 'w2_' , str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_HLVKI_', str(inj_params_bbh['eta']),'_' , 'w2_' , str(j), '.npy')))
    w3.append(np.load(file=''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_ETCE_', str(inj_params_bbh['eta']),'_' , 'w3_' , str(j), '.npy'))))
    os.remove(''.join(('datafiles/', str(Mc_bbhf),'_s_',str(distanza_l),
                                    '_ETCE_', str(inj_params_bbh['eta']),'_' , 'w3_' , str(j), '.npy')))
# In[47]:


s1=np.array(s1)
s1[:]=np.array(s1[:])
s2=np.array(s2)
s2[:]=np.array(s2[:])
s3=np.array(s3)
s3[:]=np.array(s3[:])
v1=np.array(v1)
v1[:]=np.array(v1[:])
v2=np.array(v2)
v2[:]=np.array(v2[:])
v3=np.array(v3)
v3[:]=np.array(v3[:])
w1=np.array(w1)
w1[:]=np.array(w1[:])
w2=np.array(w2)
w2[:]=np.array(w2[:])
w3=np.array(w3)
w3[:]=np.array(w3[:])
n3d1=np.array(n3d1)
n3d1[:]=np.array(n3d1[:])
n3d2=np.array(n3d2)
n3d2[:]=np.array(n3d2[:])
n3d3=np.array(n3d3)
n3d3[:]=np.array(n3d3[:])

#xx_bns=f2_bns
#xx_bbh=f2_bbh
s1_mean=np.zeros((len(f1)), dtype=np.double)
s2_mean=np.zeros((len(f2)), dtype=np.double)
s3_mean=np.zeros((len(f3)), dtype=np.double)

n3d1_mean=np.zeros((len(f1)), dtype=np.double)
n3d2_mean=np.zeros((len(f2)), dtype=np.double)
n3d3_mean=np.zeros((len(f3)), dtype=np.double)

v1_mean=np.zeros((len(f1)), dtype=np.double)
v2_mean=np.zeros((len(f2)), dtype=np.double)
v3_mean=np.zeros((len(f3)), dtype=np.double)

w1_mean=np.zeros((len(f1)), dtype=np.double)
w2_mean=np.zeros((len(f2)), dtype=np.double)
w3_mean=np.zeros((len(f3)), dtype=np.double)


s1_std=np.zeros((len(f1)), dtype=np.double)
s2_std=np.zeros((len(f2)), dtype=np.double)
s3_std=np.zeros((len(f3)), dtype=np.double)

n3d1_std=np.zeros((len(f1)), dtype=np.double)
n3d2_std=np.zeros((len(f2)), dtype=np.double)
n3d3_std=np.zeros((len(f3)), dtype=np.double)

v1_std=np.zeros((len(f1)), dtype=np.double)
v2_std=np.zeros((len(f2)), dtype=np.double)
v3_std=np.zeros((len(f3)), dtype=np.double)

w1_std=np.zeros((len(f1)), dtype=np.double)
w2_std=np.zeros((len(f2)), dtype=np.double)
w3_std=np.zeros((len(f3)), dtype=np.double)

for i in range (0, len(f3)):
    if i in range(0, len(f2)):
        s1_mean[i]=np.mean(s1[:,i])
        s2_mean[i]=np.mean(s2[:,i])
        v1_mean[i]=np.mean(v1[:,i])
        v2_mean[i]=np.mean(v2[:,i])
        w1_mean[i]=np.mean(w1[:,i])
        w2_mean[i]=np.mean(w2[:,i])
        n3d1_mean[i]=np.mean(n3d1[:,i])
        n3d2_mean[i]=np.mean(n3d2[:,i])
        
        
        s1_std[i]=np.sqrt(np.var(s1[:,i], ddof=1))
        s2_std[i]=np.sqrt(np.var(s2[:,i], ddof=1))
        v1_std[i]=np.sqrt(np.var(v1[:,i], ddof=1))
        v2_std[i]=np.sqrt(np.var(v2[:,i], ddof=1))
        w1_std[i]=np.sqrt(np.var(w1[:,i], ddof=1))
        w2_std[i]=np.sqrt(np.var(w2[:,i], ddof=1))
        n3d1_std[i]=np.sqrt(np.var(n3d1[:,i], ddof=1))
        n3d2_std[i]=np.sqrt(np.var(n3d2[:,i], ddof=1))
        #print(s1[:,i])
    n3d3_mean[i]=np.mean(n3d3[:,i])
    s3_mean[i]=np.mean(s3[:,i])
    v3_mean[i]=np.mean(v3[:,i])
    w3_mean[i]=np.mean(w3[:,i])
    s3_std[i]=np.sqrt(np.var(s3[:,i]))
    v3_std[i]=np.sqrt(np.var(v3[:,i]))
    w3_std[i]=np.sqrt(np.var(w3[:,i]))
    n3d3_std[i]=np.sqrt(np.var(n3d3[:,i]))
#print(s1_mean[np.nonzero(s1_mean)], s1_std[np.nonzero(s1_std)])
s1, s2, s3 = None, None, None
v1, v2, v3 = None, None, None
w1, w2, w3 = None, None, None
#print(len(n3d3_mean), len(n3d3_std), len(f3))
#print(n3d3_mean)


# In[48]:


x1=f1
ys1=np.array(n3d1_mean)*x1
cis1 = 3*n3d1_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd1**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the 3-detector NS HLVK_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
x1=f2
ys1=np.array(n3d2_mean)*x1
cis1 = 3*n3d2_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd2**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the 3-detector NS HLVKI_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
x1=f3
ys1=np.array(n3d3_mean)*x1
cis1 = 3*n3d3_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd3**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the 3-detector NS ETCE_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()



x1=f1
ys1=np.array(s1_mean)*x1
cis1 = 3*s1_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd1**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the scalar 4-detector NS HLVK_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
x1=f2
ys1=np.array(s2_mean)*x1
cis1 = 3*s2_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd2**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the scalar 4-detector NS HLVKI_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
x1=f3
ys1=np.array(s3_mean)*x1
cis1 = 3*s3_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd3**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the scalar 4-detector NS ETCE_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()





x1=f1
ys1=np.array(v1_mean)*x1
cis1 = 3*v1_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd1**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the V 4-detector NS HLVK_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
x1=f2
ys1=np.array(v2_mean)*x1
cis1 = 3*v2_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd2**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the V 4-detector NS HLVKI_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
x1=f3
ys1=np.array(v3_mean)*x1
cis1 = 3*v3_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd3**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the V 4-detector NS ETCE_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()





x1=f1
ys1=np.array(w1_mean)*x1
cis1 = 3*w1_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd1**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the W 4-detector NS HLVK_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
x1=f2
ys1=np.array(w2_mean)*x1
cis1 = 3*w2_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd2**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the W 4-detector NS HLVKI_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()
x1=f3
ys1=np.array(w3_mean)*x1
cis1 = 3*w3_std*x1
figs, axs = plt.subplots()
axs.plot(x1,ys1, label='PSD 3-D NS')
axs.fill_between(x1, (ys1-cis1), (ys1+cis1), color='b', alpha=.1, label='3 sigma')
plt.loglog(x1, noise_asd3**2, label='PSD sens.')
plt.xlabel('f', fontsize=10)
plt.ylabel('PSD', fontsize=10)
plt.tick_params(axis='x', labelsize=10)
plt.tick_params(axis='y', labelsize=10)
#plt.legend()
plt.legend(loc=2, prop={'size': 10})
figs.tight_layout()
plt.savefig(''.join(('PSD/PSD for the W 4-detector NS ETCE_', str(distanza_l) , '_', str(Mc_bbhf), '_', str(etazonz),'.png')))
#plt.show()
plt.close()


# In[49]:


s1_psnr_fres = s1_mean/(noise_asd1**2)
s1_psnr = df1*np.sum(s1_psnr_fres)
s2_psnr_fres = s2_mean/(noise_asd2**2)
s2_psnr = df2*np.sum(s2_psnr_fres)
s3_psnr_fres = s3_mean/(noise_asd3**2)
s3_psnr = df3*np.sum(s3_psnr_fres)
s1_psnr_fres, s2_psnr_fres, s3_psnr_fres=None, None, None
#s1_mean, s2_mean, s3_mean = None, None, None

v1_psnr_fres = v1_mean/(noise_asd1**2)
v1_psnr = df1*np.sum(v1_psnr_fres)
v2_psnr_fres = v2_mean/(noise_asd2**2)
v2_psnr = df2*np.sum(v2_psnr_fres)
v3_psnr_fres = v3_mean/(noise_asd3**2)
v3_psnr = df3*np.sum(v3_psnr_fres)
v1_psnr_fres, v2_psnr_fres, v3_psnr_fres=None, None, None
#v1_mean, v2_mean, v3_mean = None, None, None

w1_psnr_fres = w1_mean/(noise_asd1**2)
w1_psnr = df1*np.sum(w1_psnr_fres)
w2_psnr_fres = w2_mean/(noise_asd2**2)
w2_psnr = df2*np.sum(w2_psnr_fres)
w3_psnr_fres = w3_mean/(noise_asd3**2)
w3_psnr = df3*np.sum(w3_psnr_fres)
w1_psnr_fres, w2_psnr_fres, w3_psnr_fres=None, None, None
#w1_mean, w2_mean, w3_mean = None, None, None

n3d1_psnr_fres = n3d1_mean/(noise_asd1**2)
n3d1_psnr = df1*np.sum(n3d1_psnr_fres)
n3d2_psnr_fres = n3d2_mean/(noise_asd2**2)
n3d2_psnr = df2*np.sum(n3d2_psnr_fres)
n3d3_psnr_fres = n3d3_mean/(noise_asd3**2)
n3d3_psnr = df3*np.sum(n3d3_psnr_fres)
n3d1_psnr_fres, n3d2_psnr_fres, n3d3_psnr_fres=None, None, None
#n3d1_mean, n3d2_mean, n3d3_mean = None, None, None














s1_psnr_freserrm = (s1_mean-3*s1_std)/(noise_asd1**2)
s1_psnrerrm = df1*np.sum(s1_psnr_freserrm)
s2_psnr_freserrm = (s2_mean-3*s2_std)/(noise_asd2**2)
s2_psnrerrm = df2*np.sum(s2_psnr_freserrm)
s3_psnr_freserrm = (s3_mean-3*s3_std)/(noise_asd3**2)
s3_psnrerrm = df3*np.sum(s3_psnr_freserrm)
s1_psnr_freserrm, s2_psnr_freserrm, s3_psnr_freserrm=None, None, None

v1_psnr_freserrm = (v1_mean-3*v1_std)/(noise_asd1**2)
v1_psnrerrm = df1*np.sum(v1_psnr_freserrm)
v2_psnr_freserrm = (v2_mean-3*v2_std)/(noise_asd2**2)
v2_psnrerrm = df2*np.sum(v2_psnr_freserrm)
v3_psnr_freserrm = (v3_mean-3*v3_std)/(noise_asd3**2)
v3_psnrerrm = df3*np.sum(v3_psnr_freserrm)
v1_psnr_freserrm, v2_psnr_freserrm, v3_psnr_freserrm=None, None, None

w1_psnr_freserrm = (w1_mean-3*w1_std)/(noise_asd1**2)
w1_psnrerrm = df1*np.sum(w1_psnr_freserrm)
w2_psnr_freserrm = (w2_mean-3*w2_std)/(noise_asd2**2)
w2_psnrerrm = df2*np.sum(w2_psnr_freserrm)
w3_psnr_freserrm = (w3_mean-3*w3_std)/(noise_asd3**2)
w3_psnrerrm = df3*np.sum(w3_psnr_freserrm)
w1_psnr_freserrm, w2_psnr_freserrm, w3_psnr_freserrm=None, None, None

n3d1_psnr_freserrm = (n3d1_mean-3*n3d1_std)/(noise_asd1**2)
n3d1_psnrerrm = df1*np.sum(n3d1_psnr_freserrm)
n3d2_psnr_freserrm = (n3d2_mean-3*n3d2_std)/(noise_asd2**2)
n3d2_psnrerrm = df2*np.sum(n3d2_psnr_freserrm)
n3d3_psnr_freserrm = (n3d3_mean-3*n3d3_std)/(noise_asd3**2)
n3d3_psnrerrm = df3*np.sum(n3d3_psnr_freserrm)
n3d1_psnr_freserrm, n3d2_psnr_freserrm, n3d3_psnr_freserrm=None, None, None










s1_psnr_freserr = (s1_mean+3*s1_std)/(noise_asd1**2)
s1_psnrerr = df1*np.sum(s1_psnr_freserr)
s2_psnr_freserr = (s2_mean+3*s2_std)/(noise_asd2**2)
s2_psnrerr = df2*np.sum(s2_psnr_freserr)
s3_psnr_freserr = (s3_mean+3*s3_std)/(noise_asd3**2)
s3_psnrerr = df3*np.sum(s3_psnr_freserr)
s1_psnr_freserr, s2_psnr_freserr, s3_psnr_freserr=None, None, None

v1_psnr_freserr = (v1_mean+3*v1_std)/(noise_asd1**2)
v1_psnrerr = df1*np.sum(v1_psnr_freserr)
v2_psnr_freserr = (v2_mean+3*v2_std)/(noise_asd2**2)
v2_psnrerr = df2*np.sum(v2_psnr_freserr)
v3_psnr_freserr = (v3_mean+3*v3_std)/(noise_asd3**2)
v3_psnrerr = df3*np.sum(v3_psnr_freserr)
v1_psnr_freserr, v2_psnr_freserr, v3_psnr_freserr=None, None, None

w1_psnr_freserr = (w1_mean+3*w1_std)/(noise_asd1**2)
w1_psnrerr = df1*np.sum(w1_psnr_freserr)
w2_psnr_freserr = (w2_mean+3*w2_std)/(noise_asd2**2)
w2_psnrerr = df2*np.sum(w2_psnr_freserr)
w3_psnr_freserr = (w3_mean+3*w3_std)/(noise_asd3**2)
w3_psnrerr = df3*np.sum(w3_psnr_freserr)
w1_psnr_freserr, w2_psnr_freserr, w3_psnr_freserr=None, None, None

n3d1_psnr_freserr = (n3d1_mean+3*n3d1_std)/(noise_asd1**2)
n3d1_psnrerr = df1*np.sum(n3d1_psnr_freserr)
n3d2_psnr_freserr = (n3d2_mean+3*n3d2_std)/(noise_asd2**2)
n3d2_psnrerr = df2*np.sum(n3d2_psnr_freserr)
n3d3_psnr_freserr = (n3d3_mean+3*n3d3_std)/(noise_asd3**2)
n3d3_psnrerr = df3*np.sum(n3d3_psnr_freserr)
n3d1_psnr_freserr, n3d2_psnr_freserr, n3d3_psnr_freserr=None, None, None




sys.stdout.flush()
print('HLVK')
print('SNR1 (3 dets.)= ',n3d1_psnr, 'f_bins= ', len(f1), 'f_av_SNR= ', n3d1_psnr/len(f1))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "HLVK_dev_s1_3dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl1), '   ', str(err_m1), '   ', 
                            str(err_eta1), '   ' , str(area1), '  ' , str(len(f1)), '    ', str(n3d1_psnr), '    ',
                            str(n3d1_psnr/len(f1)) , '    ' , str(n3d1_psnrerr), '  ', 
                            str(n3d1_psnrerr/len(f1)) , '    ' , str(n3d1_psnrerrm), '  ', str(n3d1_psnrerrm/len(f1)),'\n')))
print('SNRs (4 dets.)= ',s1_psnr, 'f_bins= ', len(f1), 'f_av_SNR= ', s1_psnr/len(f1))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "HLVK_dev_s1_s4dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl1), '   ', str(err_m1), '   ', 
                            str(err_eta1), '   ' , str(area1), '  ' , str(len(f1)), '    ', str(s1_psnr), '    ',
                            str(s1_psnr/len(f1)) , '    ' , str(s1_psnrerr), '  ', 
                            str(s1_psnrerr/len(f1)) , '    ' , str(s1_psnrerrm), '  ', str(s1_psnrerrm/len(f1)),'\n')))
print('SNRv (4 dets.)= ',v1_psnr, 'f_bins= ', len(f1), 'f_av_SNR= ', v1_psnr/len(f1))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "HLVK_dev_s1_v4dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl1), '   ', str(err_m1), '   ', 
                            str(err_eta1), '   ' , str(area1), '  ' , str(len(f1)), '    ', str(v1_psnr), '    ',
                            str(v1_psnr/len(f1)) , '    ' , str(v1_psnrerr), '  ', 
                            str(v1_psnrerr/len(f1)) , '    ' , str(v1_psnrerrm), '  ', str(v1_psnrerrm/len(f1)),'\n')))
print('SNRw (4 dets.)= ',w1_psnr, 'f_bins= ', len(f1), 'f_av_SNR= ', w1_psnr/len(f1))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "HLVK_dev_s1_w4dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl1), '   ', str(err_m1), '   ', 
                            str(err_eta1), '   ' , str(area1), '  ' , str(len(f1)), '    ', str(w1_psnr), '    ',
                            str(w1_psnr/len(f1)) , '    ' , str(w1_psnrerr), '  ', 
                            str(w1_psnrerr/len(f1)) , '    ' , str(w1_psnrerrm), '  ', str(w1_psnrerrm/len(f1)),'\n')))





sys.stdout.flush()
print('HLVKI')
print('SNR1 (3 dets.)= ',n3d2_psnr, 'f_bins= ', len(f2), 'f_av_SNR= ', n3d2_psnr/len(f2))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "HLVKI_dev_s1_3dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl2), '   ', str(err_m2), '   ', 
                            str(err_eta2), '   ' , str(area2), '  ' , str(len(f2)), '    ', str(n3d2_psnr), '    ',
                            str(n3d2_psnr/len(f2)) , '    ' , str(n3d2_psnrerr), '  ', 
                            str(n3d2_psnrerr/len(f2)) , '    ' , str(n3d2_psnrerrm), '  ', str(n3d2_psnrerrm/len(f2)),'\n')))
print('SNRs (4 dets.)= ',s2_psnr, 'f_bins= ', len(f2), 'f_av_SNR= ', s2_psnr/len(f2))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "HLVKI_dev_s1_s4dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl2), '   ', str(err_m2), '   ', 
                            str(err_eta2), '   ' , str(area2), '  ' , str(len(f2)), '    ', str(s2_psnr), '    ',
                            str(s2_psnr/len(f2)) , '    ' , str(s2_psnrerr), '  ', 
                            str(s2_psnrerr/len(f2)) , '    ' , str(s2_psnrerrm), '  ', str(s2_psnrerrm/len(f2)),'\n')))
print('SNRv (4 dets.)= ',v2_psnr, 'f_bins= ', len(f2), 'f_av_SNR= ', v2_psnr/len(f2))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "HLVKI_dev_s1_v4dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl2), '   ', str(err_m2), '   ', 
                            str(err_eta2), '   ' , str(area2), '  ' , str(len(f2)), '    ', str(v2_psnr), '    ',
                            str(v2_psnr/len(f2)) , '    ' , str(v2_psnrerr), '  ', 
                            str(v2_psnrerr/len(f2)) , '    ' , str(v2_psnrerrm), '  ', str(v2_psnrerrm/len(f2)),'\n')))
print('SNRw (4 dets.)= ',w2_psnr, 'f_bins= ', len(f2), 'f_av_SNR= ', w2_psnr/len(f2))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "HLVKI_dev_s1_w4dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl2), '   ', str(err_m2), '   ', 
                            str(err_eta2), '   ' , str(area2), '  ' , str(len(f2)), '    ', str(w2_psnr), '    ',
                            str(w2_psnr/len(f2)) , '    ' , str(w2_psnrerr), '  ', 
                            str(w2_psnrerr/len(f2)) , '    ' , str(w2_psnrerrm), '  ', str(w2_psnrerrm/len(f2)),'\n')))
    
    

    


sys.stdout.flush()
print('ETCE')
print('SNR1 (3 dets.)= ',n3d3_psnr, 'f_bins= ', len(f3), 'f_av_SNR= ', n3d3_psnr/len(f3))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "ETCE_dev_s1_3dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl3), '   ', str(err_m3), '   ', 
                            str(err_eta3), '   ' , str(area3), '  ' , str(len(f3)), '    ', str(n3d3_psnr), '    ',
                            str(n3d3_psnr/len(f3)) , '    ' , str(n3d3_psnrerr), '  ', 
                            str(n3d3_psnrerr/len(f3)) , '    ' , str(n3d3_psnrerrm), '  ', str(n3d3_psnrerrm/len(f3)),'\n')))
print('SNRs (4 dets.)= ',s3_psnr, 'f_bins= ', len(f3), 'f_av_SNR= ', s3_psnr/len(f3))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "ETCE_dev_s1_s4dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl3), '   ', str(err_m3), '   ', 
                            str(err_eta3), '   ' , str(area3), '  ' , str(len(f3)), '    ', str(s3_psnr), '    ',
                            str(s3_psnr/len(f3)) , '    ' , str(s3_psnrerr), '  ', 
                            str(s3_psnrerr/len(f3)) , '    ' , str(s3_psnrerrm), '  ', str(s3_psnrerrm/len(f3)),'\n')))
print('SNRv (4 dets.)= ',v3_psnr, 'f_bins= ', len(f3), 'f_av_SNR= ', v3_psnr/len(f3))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "ETCE_dev_s1_v4dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl3), '   ', str(err_m3), '   ', 
                            str(err_eta3), '   ' , str(area3), '  ' , str(len(f3)), '    ', str(v3_psnr), '    ',
                            str(v3_psnr/len(f3)) , '    ' , str(v3_psnrerr), '  ', 
                            str(v3_psnrerr/len(f3)) , '    ' , str(v3_psnrerrm), '  ', str(v3_psnrerrm/len(f3)),'\n')))
print('SNRw (4 dets.)= ',w3_psnr, 'f_bins= ', len(f3), 'f_av_SNR= ', w3_psnr/len(f3))
with open("".join(('dl/' , str(Mc_bbhf), ' ', str(etazonz), '_', "ETCE_dev_s1_w4dets.txt")), 'a') as datafile:
    datafile.write(''.join((str(distanza_l), '   ', str(err_dl3), '   ', str(err_m3), '   ', 
                            str(err_eta3), '   ' , str(area3), '  ' , str(len(f3)), '    ', str(w3_psnr), '    ',
                            str(w3_psnr/len(f3)) , '    ' , str(w3_psnrerr), '  ', 
                            str(w3_psnrerr/len(f3)) , '    ' , str(w3_psnrerrm), '  ', str(w3_psnrerrm/len(f3)),'\n')))
 
    

# In[ ]:





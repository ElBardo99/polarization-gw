import numpy as np
import matplotlib.pyplot as plt
import scipy
import time
import sys, os
import re
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
def interpolate_nan(array_like):
    array = array_like.copy()

    nans = np.isnan(array)

    def get_x(a):
        return a.nonzero()[0]

    array[nans] = np.interp(get_x(nans), get_x(~nans), array[~nans])

    return array



array_m =  np.logspace(0.0, 20, 30, base=1.2 )
#for i in range (0, len(array_m)-1):
#    print(np.log10(array_m[i+1]) - np.log10(array_m[i]))
array_m_log = np.log(array_m)
array_m_log10 = np.log10(array_m)
array_eta1 = np.arange(0.0, 14*0.017 , 0.017)
array_eta = np.zeros_like(array_eta1)
array_eta[:] = 0.249
array_eta[:] -= array_eta1[:]
array_eta = np.flip(array_eta)
#print(array_eta, array_m)
def setup_plot(figsize, xlabel, ylabel):
    """ @brief Initialize basic plot parameters
        @param figsize Figure size
        @param xlabel X-axis label
        @param ylabel Y-axis label
    """

    plt.figure(figsize=figsize)

    #print(arr_6_3g_bns_vw.shape)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
def heatmap2dlog(arr, str):
    setup_plot((6,2),'Eta', 'Log(Mass)')
    #extent=[0,np.maximum(dets_coordinates2[:,:,0,2].reshape((len(decls)*len(ras),))),0,np.maximum(dets_coordinates2[:,:,0,3].reshape((len(decls)*len(ras),)))]
    plt.imshow(arr, origin='lower', extent=[array_eta[0], array_eta[len(array_eta)-1] , array_m_log[0], array_m_log[len(array_m_log)-1]] , cmap='viridis')

    plt.colorbar()
    #plt.savefig(str)
    plt.close()

    fig, ax = plt.subplots()
    ax.set_box_aspect(1)
    ax.imshow(arr, origin='lower',  cmap='viridis')
    #print(array_eta[0], array_eta[len(array_eta)-1] , array_m_log[0], array_m_log[len(array_m_log)-1])
    plt.savefig(''.join((str, '.png')) )
    plt.close()
def heatmap2dlog10(arr, str):

    fig, ax = plt.subplots()
    #ax.set_box_aspect(arr.shape[0]/arr.shape[1])
    im = ax.imshow(arr, origin='lower', extent=[array_eta[0], array_eta[len(array_eta)-1] , array_m_log10[0], array_m_log10[len(array_m_log10)-1]], cmap='viridis', aspect="auto")
    plt.xlabel('η', fontsize=10)
    plt.ylabel('log(Mc)', fontsize=10)
    plt.tick_params(axis='x', labelsize=10)
    plt.tick_params(axis='y', labelsize=10)
    #plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    fig.colorbar(im, cax=cax)
    fig.tight_layout()
    plt.savefig(''.join((str, '.png')) )
    plt.close()
    #levels = np.linspace(-11, -4, 4 )
    #norm = cm.colors.Normalize(vmax=abs(arr).max(), vmin=-abs(arr).max())
    #cmap = cm.PRGn
    xlist = array_eta
    ylist = array_m
    X, Y = np.meshgrid(xlist, ylist)
    Z = arr
    fig,ax=plt.subplots()
    cp = ax.contourf(X, Y, Z)
    plt.xlabel('η', fontsize=10)
    plt.ylabel('log(Mc)', fontsize=10)
    plt.tick_params(axis='x', labelsize=10)
    plt.tick_params(axis='y', labelsize=10)
    #plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    fig.colorbar(cp, cax=cax) # Add a colorbar to a plot
    #ax.set_title('Filled Contours Plot')
    #ax.set_xlabel('x (cm)')
    #ax.set_ylabel('y (cm)')
    fig.tight_layout()
    plt.savefig(''.join((str, '_cont.png')) )
    plt.close()

def quickcomp(a,b):
    if a/b > 0.999 and a/b < 1.001:
        return True
    else:
        return False
def match(arr, m):
    i = 0
    while i in range (0, len(arr)):
        bool = quickcomp(arr[i], m)
        if bool:
            return int(i)
            break
        i +=1



class DataArr(object):
    def __init__(self, name):

        self.name = str(name)
        self.array = np.full((30,14,7), np.nan)
        string1 = (self.name).split('_')
        self.n=string1[1]
        self.net=string1[2]
        print(self.net)
        self.cbc=string1[3]
        self.inj=string1[4]
        str_net = self.net
    def get_n(self):
        return self.n
    def get_net(self):
        return self.net
    def get_cbc(self):
        return self.cbc
    def get_inj(self):
        return self.inj

    def get_data(self):
        str_net = self.net
        inj_dict={
        'tensor':'n',
        'scalar':'s',
        'vectorv':'v',
        'vectorw':'w',
        'vectorvw':'vw'
        }
        f= open(''.join(('meta_g100jan/', str(self.inj), '/100.0_', str_net.upper(),
        '_dev_', inj_dict[self.inj], 'meta_', self.n, '.txt')), 'r')
        #f1= open(''.join(('meta_g100jan/',self.net,'/',self.inj, '/', other_cbc,
        #    '/100.0_', str_net.upper(), '_meta_', str_ss, '_SNR', self.n, '.txt')), 'r')
        arr_str=str(f.read()).split("\n")
        arr_str.pop()
        #print(arr_str[0])

        arr=self.array
        #print(np.isnan(arr))
        for j in range (0, len(arr_str)):
            arr_str[j]=re.split("\s+", arr_str[j])
            for ob in arr_str[j]:
                arr_str[j][arr_str[j].index(ob)] = np.float64(ob)
            #print( np.isnan(arr_str[j]))
                #print(j, "  ok")
            work_arr , arr_str[j] = np.array(arr_str[j]) , np.array(arr_str[j])
            #print(arr_str[j], len(arr_str[j]))
            index_m =  match(array_m, work_arr[0])
            index_eta = match(array_eta, work_arr[1])

            arr[index_m , index_eta , 0:7] = work_arr[2:9]
            #arr[index_m , index_eta, 6:8]  = np.log(work_arr[7:9])
        arr_str=np.array(arr_str)
        #print(arr)

        #print( np.isnan(arr))
        '''
        arr_str1=str(f1.read()).split("\n")
        arr_str1.pop()
        #print(arr_str[0])

        #arr1=self.array

        for j in range (0, len(arr_str1)):
            arr_str1[j]=re.split("\s+", arr_str1[j])
            for ob in arr_str1[j]:
                arr_str1[j][arr_str1[j].index(ob)] = np.float64(ob)
                #print(j, "  ok")
            work_arr1 , arr_str1[j] = np.array(arr_str1[j]) , np.array(arr_str1[j])
            #print(arr_str[j], len(arr_str[j]))
            index_m1 =  match(array_m, work_arr1[0])
            index_eta1 = match(array_eta, work_arr1[1])

            arr[index_m1 , index_eta1 , 0:6] = work_arr1[2:8]
            arr[index_m1 , index_eta1, 6:8]  = np.log(work_arr1[4:6])
        arr_str1=np.array(arr_str1)
        #print( np.isnan(arr))

        #print(np.float64(abs(np.float64(arr[:,:,4])-np.float64(arr[:,:,2])))/3.0)
        '''

        list_names = ['err_dl','err_m','err_eta', 'area_90', 'length' , 'SNR', 'std']
        for i in range (0, 7):
            arr_c =arr[:,:,i]
            #print( np.isnan(arr_c))
            #print(arr_c.shape)
            '''
            for a in range (0, arr_c.shape[0]):
                #print(array_m[a])
                #print( np.isnan(arr_c[a,:]))
                for b in range (0, arr_c.shape[1]):
                    #print(b)
                    #print( np.isnan(arr_c[a,b]))
                    if self.n == "2" and i==2:
                        errarr=np.zeros_like(arr_c)
                        errarr[:,:] = arr_c[:,:]/10
                        np.save(''.join(('leonardo/meta/',self.net,'/',self.inj, '/', self.cbc,
                            '/100.0_', str_net.upper(), '_meta_', str_ss, '_ERRORE_ARR')), errarr)
                    if np.isnan(arr_c[a,b]) and array_m[a] > 6.0:
                        #print('found!!!')

                        arr_c[a,b] = np.inf
                        #print('found')
            '''
            arr_c = interpolate_nan(arr_c)
            name = ''.join(('meta_g100jan/ffiles/',
                '/100.0_', str_net.upper(), '_meta_', inj_dict[self.inj], '_', self.n, '_', list_names[i], '_log'))

            name10 = ''.join(('meta_g100jan/ffiles/',
                '/100.0_', str_net.upper(), '_meta_', inj_dict[self.inj], '_', self.n, '_', list_names[i], '_log10'))
            if os.path.exists(''.join((name10, '_only_mass.txt'))):
                os.remove(''.join((name10, '_only_mass.txt')))
            if os.path.exists(''.join((name10, '_only_eta.txt'))):
                os.remove(''.join((name10, '_only_eta.txt')))
            if os.path.exists(''.join((name10, '_metaSINERR.txt'))):
                os.remove(''.join((name10, '_metaSINERR.txt')))
            if os.path.exists(''.join((name10, '_metaSINERR.txt'))):
                os.remove(''.join((name10, '_metaSINERR.txt')))
            if os.path.exists(''.join((name10, '_metawitERR.txt'))):
                os.remove(''.join((name10, '_metawitERR.txt')))
            for b in range (0, arr_c.shape[1]):
                barr=np.zeros((arr_c.shape[0]))
                for a in range (0, arr_c.shape[0]):
                    if arr_c[a,b] == np.inf:
                        arr_c[a,b] = np.nan
                    if arr_c[a,b] != np.inf and arr_c[a,b] != np.nan:
                        if str(arr_c[a,b]) != 'nan':
                            if b == (arr_c.shape[1]-1):
                                with open(''.join((name10, '_only_mass.txt')), 'a') as datafile:
                                    datafile.write(''.join((str(array_m[a]), '   ',  str(arr_c[a,b]), '\n' )))
                            if a == (arr_c.shape[0] -1):
                                with open(''.join((name10, '_only_eta.txt')), 'a') as datafile:
                                    datafile.write(''.join((str(array_eta[b]), '   ',  str(arr_c[a,b]), '\n' )))

                            with open(''.join((name10, '_metaSINERR.txt')), 'a') as datafile:
                                datafile.write(''.join((str(array_m[a]), '   ', str(array_eta[b]), '   ',  str(arr_c[a,b]),'\n' )))
                            if list_names[i]=='SNR':
                                with open(''.join((name10, '_metawitERR.txt')), 'a') as datafile:
                                    datafile.write(''.join((str(array_m[a]), '   ', str(array_eta[b]), '   ',  str(arr_c[a,b]),
                                    '       ', str(arr[a,b,i+1]) ,'\n' )))
                                #print('found again')
            #print(np.isnan(arr_c))
                #if self.n not in ('0', '1', '2', '3'):
                #    errore=np.load(file=''.join(('leonardo/meta/',self.net,'/',self.inj, '/', self.cbc,
                #        '/100.0_', str_net.upper(), '_meta_', str_ss, '_ERRORE_ARR.npy')))
                barr = arr_c[:,b]
                if os.path.exists(''.join((name10, '_ETA_CONST_', str(array_eta[b]) ,'_SINERR.txt'))):
                    os.remove(''.join((name10, '_ETA_CONST_', str(array_eta[b]) ,'_SINERR.txt')))
                for a in range (0, arr_c.shape[0]):
                    if not np.isnan(barr[a]):
                        with open(''.join((name10, '_ETA_CONST_', str(array_eta[b]) ,'_SINERR.txt')), 'a') as datafile:
                            datafile.write(''.join((str(array_m[a]), '  ', str(barr[a]), '\n')))
                if list_names[i] == 'SNR':
                    if os.path.exists(''.join((name10, '_ETA_CONST_', str(array_eta[b]) ,'_witERR.txt'))):
                        os.remove(''.join((name10, '_ETA_CONST_', str(array_eta[b]) ,'_witERR.txt')))
                    for a in range (0, arr_c.shape[0]):
                        if not np.isnan(barr[a]):
                            with open(''.join((name10, '_ETA_CONST_', str(array_eta[b]) ,'_witERR.txt')), 'a') as datafile:

                                datafile.write(''.join((str(array_m[a]), '  ', str(barr[a]), '  ', str(arr[a,b,i+1]), '\n')))

            heatmap2dlog(arr_c, name)
            heatmap2dlog10(arr_c, name10)
        print('Fatto ', self.name)
            #print(arr_c.shape)

            #for k in range (0, 10):
arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvk_bbh_scalar')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvk_bbh_scalar')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvk_bbh_scalar')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvk_bbh_scalar')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvk_bbh_tensor')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvk_bbh_tensor')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvk_bbh_tensor')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvk_bbh_tensor')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvk_bbh_vectorv')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvk_bbh_vectorv')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvk_bbh_vectorv')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvk_bbh_vectorv')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvk_bbh_vectorw')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvk_bbh_vectorw')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvk_bbh_vectorw')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvk_bbh_vectorw')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvk_bbh_vectorvw')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvk_bbh_vectorvw')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvk_bbh_vectorvw')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvk_bbh_vectorvw')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvki_bbh_scalar')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvki_bbh_scalar')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvki_bbh_scalar')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvki_bbh_scalar')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvki_bbh_tensor')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvki_bbh_tensor')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvki_bbh_tensor')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvki_bbh_tensor')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvki_bbh_vectorv')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvki_bbh_vectorv')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvki_bbh_vectorv')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvki_bbh_vectorv')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvki_bbh_vectorw')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvki_bbh_vectorw')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvki_bbh_vectorw')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvki_bbh_vectorw')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_hlvki_bbh_vectorvw')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_hlvki_bbh_vectorvw')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_hlvki_bbh_vectorvw')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_hlvki_bbh_vectorvw')
arr_4_hlvk_bbh_s1.get_data()






arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_etce_bbh_scalar')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_etce_bbh_scalar')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_etce_bbh_scalar')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_etce_bbh_scalar')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_etce_bbh_tensor')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_etce_bbh_tensor')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_etce_bbh_tensor')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_etce_bbh_tensor')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_etce_bbh_vectorv')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_etce_bbh_vectorv')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_etce_bbh_vectorv')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_etce_bbh_vectorv')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_etce_bbh_vectorw')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_etce_bbh_vectorw')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_etce_bbh_vectorw')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_etce_bbh_vectorw')
arr_4_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('arr_3dets_etce_bbh_vectorvw')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('arr_s4dets_etce_bbh_vectorvw')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('arr_v4dets_etce_bbh_vectorvw')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('arr_w4dets_etce_bbh_vectorvw')
arr_4_hlvk_bbh_s1.get_data()

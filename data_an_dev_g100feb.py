import numpy as np
import matplotlib.pyplot as plt
import scipy
import time
import sys, os
import re
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
def interpolate_nan(array_like):
    array = array_like.copy()

    nans = np.isnan(array)

    def get_x(a):
        return a.nonzero()[0]

    array[nans] = np.interp(get_x(nans), get_x(~nans), array[~nans])

    return array



array_m =  np.logspace(0.0, 20, 30, base=1.2 )
#for i in range (0, len(array_m)-1):
#    print(np.log10(array_m[i+1]) - np.log10(array_m[i]))
array_m_log = np.log(array_m)
array_m_log10 = np.log10(array_m)
array_eta1 = np.arange(0.0, 14*0.017 , 0.017)
array_eta = np.zeros_like(array_eta1)
array_eta[:] = 0.249
array_eta[:] -= array_eta1[:]
array_eta = np.flip(array_eta)
array_d = np.logspace(1.0, 3.7, 30, base=10)
#print(array_eta, array_m)
def setup_plot(figsize, xlabel, ylabel):
    """ @brief Initialize basic plot parameters
        @param figsize Figure size
        @param xlabel X-axis label
        @param ylabel Y-axis label
    """

    plt.figure(figsize=figsize)


    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
#print(arr_6_3g_bns_vw.shape)
def heatmap2dlog(arr, str):
    setup_plot((6,2),'Eta', 'Log(Mass)')
    #extent=[0,np.maximum(dets_coordinates2[:,:,0,2].reshape((len(decls)*len(ras),))),0,np.maximum(dets_coordinates2[:,:,0,3].reshape((len(decls)*len(ras),)))]
    plt.imshow(arr, origin='lower', extent=[array_eta[0], array_eta[len(array_eta)-1] , array_m_log[0], array_m_log[len(array_m_log)-1]] , cmap='viridis')

    plt.colorbar()
    #plt.savefig(str)
    plt.close()

    fig, ax = plt.subplots()
    ax.set_box_aspect(1)
    ax.imshow(arr, origin='lower',  cmap='viridis')
    #print(array_eta[0], array_eta[len(array_eta)-1] , array_m_log[0], array_m_log[len(array_m_log)-1])
    plt.savefig(''.join((str, '.png')) )
    plt.close()
def heatmap2dlog10(arr, str):

    fig, ax = plt.subplots()
    #ax.set_box_aspect(arr.shape[0]/arr.shape[1])
    im = ax.imshow(arr, origin='lower', extent=[array_eta[0], array_eta[len(array_eta)-1] , array_m_log10[0], array_m_log10[len(array_m_log10)-1]], cmap='viridis', aspect="auto")
    plt.xlabel('η', fontsize=10)
    plt.ylabel('log(Mc)', fontsize=10)
    plt.tick_params(axis='x', labelsize=10)
    plt.tick_params(axis='y', labelsize=10)
    #plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    fig.colorbar(im, cax=cax)
    fig.tight_layout()
    plt.savefig(''.join((str, '.png')) )
    plt.close()
    #levels = np.linspace(-11, -4, 4 )
    #norm = cm.colors.Normalize(vmax=abs(arr).max(), vmin=-abs(arr).max())
    #cmap = cm.PRGn
    xlist = array_eta
    ylist = array_m
    X, Y = np.meshgrid(xlist, ylist)
    Z = arr
    fig,ax=plt.subplots()
    cp = ax.contourf(X, Y, Z)
    plt.xlabel('η', fontsize=10)
    plt.ylabel('log(Mc)', fontsize=10)
    plt.tick_params(axis='x', labelsize=10)
    plt.tick_params(axis='y', labelsize=10)
    #plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    fig.colorbar(cp, cax=cax) # Add a colorbar to a plot
    #ax.set_title('Filled Contours Plot')
    #ax.set_xlabel('x (cm)')
    #ax.set_ylabel('y (cm)')
    fig.tight_layout()
    plt.savefig(''.join((str, '_cont.png')) )
    plt.close()

def quickcomp(a,b):
    if a/b > 0.99 and a/b < 1.01:
        return True
    else:
        return False
def match(arr, m):
    i = 0
    while i in range (0, len(arr)):
        bool = quickcomp(arr[i], m)
        if bool:
            return int(i)
            break
        i +=1



class DataArr(object):
    def __init__(self, name):

        self.name = str(name)
        self.array = np.zeros((30, 7))
        string1 = (self.name).split(' ')
        self.m=string1[0]
        self.rest=string1[1]
        string2 = (self.rest).split('_')
        self.eta = string2[0]
        self.net = string2[1]
        self.inj = string2[3]
        if self.inj == 'sds':
            if self.net == 'ETCE':
                self.inj = 's1&sdip'
            elif self.net == 'HLVK':
                self.inj = 's1sdip'
        self.k = string2[4]
        #self.a = string2[2]
        injlist = ['ndl', 'sdl', 'vdl', 'vwdl' , 'wdl' ]
        folders = ['tensor', 'scalar', 'vector_v', 'vector_vw', 'vector_w']
        self.folder = folders[injlist.index(self.inj)]
    def get_data(self):

        f= open(''.join(('dl_g100feb/',self.folder, '/', self.m,' ',self.eta, '_', self.net,
            '_', 'dev', '_', self.inj, '_', self.k, '.txt')), 'r')

        arr_str=str(f.read()).split("\n")
        arr_str.pop()
        #print(arr_str[0])

        arr=self.array
        #print(np.isnan(arr))
        for j in range (0, len(arr_str)):
            arr_str[j]=re.split("\s+", arr_str[j])
            for ob in arr_str[j]:
                arr_str[j][arr_str[j].index(ob)] = np.float64(ob)
            #print( np.isnan(arr_str[j]))
                #print(j, "  ok")
            work_arr , arr_str[j] = np.array(arr_str[j]) , np.array(arr_str[j])
            #print(arr_str[j], len(arr_str[j]))
            index_d =  match(array_d, work_arr[0])


            arr[index_d , 0:7] = work_arr[1:8]

        arr_str=np.array(arr_str)
        #print(arr)



        list_names = ['err_dl', 'err_m' , 'err_eta' , 'area_90','n_freq_bins','SNR', 'SNR_std']
        for i in range (0, 7):
            arr_c =arr[:,i]
            #print( np.isnan(arr_c))
            #print(arr_c.shape)
            for a in range (0, arr_c.shape[0]):
                #print(array_m[a])
                #print( np.isnan(arr_c[a,:]))

                if np.isnan(arr_c[a]) :
                        #print('found!!!')

                    arr_c[a] = np.inf
                        #print('found')
            arr_c = interpolate_nan(arr_c)
            name = ''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_', list_names[i], '_', self.k, '.txt'))

            namelog = ''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_log_', list_names[i], '_', self.k, '.txt'))

            namelog10 = ''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_log10_', list_names[i], '_', self.k, '.txt'))
            if os.path.exists(name):
                os.remove(name)
            if os.path.exists(namelog):
                os.remove(namelog)
            if os.path.exists(namelog10):
                os.remove(namelog10)

            if os.path.exists(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_log10_', list_names[i], '_', self.k, 'witERR.txt'))):
                os.remove(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                    '_DEV_log10_', list_names[i], '_', self.k, 'witERR.txt')))
            '''
            if os.path.exists(''.join(('newmeta/g100/dl/',self.net,'_',self.inj, '_', self.a,
                '_DEV_log10_', list_names[i], '_', self.k, 'witERR_FAKE.txt'))):
                os.remove(''.join(('newmeta/g100/dl/',self.net,'_',self.inj, '_', self.a,
                    '_DEV_log10_', list_names[i], '_', self.k, 'witERR_FAKE.txt')))
            if os.path.exists(''.join(('newmeta/g100/dl/',self.net,'_',self.inj, '_', self.a,
                '_DEV_log10_', list_names[i], '_', self.k, '_FAKE.txt'))):
                os.remove(''.join(('newmeta/g100/dl/',self.net,'_',self.inj, '_', self.a,
                    '_DEV_log10_', list_names[i], '_', self.k, '_FAKE.txt')))
            if os.path.exists(''.join(('newmeta/g100/dl/',self.net,'_',self.inj, '_', self.a,
                '_DEV_log10_', list_names[i], '_', self.k, 'witERRlog_FAKE.txt'))):
                os.remove(''.join(('newmeta/g100/dl/',self.net,'_',self.inj, '_', self.a,
                    '_DEV_log10_', list_names[i], '_', self.k, 'witERRlog_FAKE.txt')))
            '''
            if os.path.exists(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_log10_', list_names[i], '_', self.k, 'witERRlog.txt'))):
                os.remove(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                    '_DEV_log10_', list_names[i], '_', self.k, 'witERRlog.txt')))

            for a in range (0, arr_c.shape[0]):

                if arr_c[a] == np.inf:
                    arr_c[a] = np.nan
                if arr_c[a] != np.inf and arr_c[a] != np.nan:
                    if str(arr_c[a]) != 'nan' and arr_c[a] > 0.0:
                        with open(name, 'a') as datafile:
                            datafile.write(''.join((str(array_d[a]), '   ', str(arr_c[a]),'\n' )))
                        with open(namelog10, 'a') as datafile:
                            datafile.write(''.join((str(np.log10(array_d[a])), '   ', str(np.log10(arr_c[a])),'\n' )))

                        with open(namelog, 'a') as datafile:
                            datafile.write(''.join((str(np.log(array_d[a])), '   ', str(np.log(arr_c[a])),'\n' )))


                        if list_names[i]=='SNR':
                            with open(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                                '_DEV_log10_', list_names[i], '_', self.k, 'witERR.txt')), 'a') as datafile:
                                datafile.write(''.join((str(np.log10(array_d[a])), '   ', str(np.log10(arr_c[a])), '    ', str(abs(arr[a,i+1])/(arr_c[a]*np.log(10.0))) ,'\n' )))
                            with open(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                                '_DEV_', list_names[i], '_', self.k, 'witERR.txt')), 'a') as datafile:
                                datafile.write(''.join((str(array_d[a]), '   ', str(arr_c[a]),  '   ',str(arr[a,i+1]), '    ', str(arr[a,i+1]) ,'\n' )))
                            errp = np.log10(arr_c[a]+abs(arr[a,i+1]))-np.log10(arr_c[a])
                            errm = -np.log10(arr_c[a]-abs(arr[a,i+1]))+np.log10(arr_c[a])
                            with open(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                                '_DEV_log10_', list_names[i], '_', self.k, 'witERRlog.txt')), 'a') as datafile:
                                datafile.write(''.join((str(np.log10(array_d[a])), '   ', str(np.log10(arr_c[a])), '    ', str(np.nan_to_num(errm, nan=10.0)), ' ', str(np.nan_to_num(errp, nan=10.0)) ,'\n' )))

                            '''
                            if self.net == 'HLVK':
                                mean = np.random.normal(np.log10(arr_c[a]), np.sqrt(4/5)*abs(arr[a,2]-arr_c[a])/(3.0*arr_c[a]*np.log(10.0)))
                                mean10 = 10**mean

                                errpf = np.log10(mean10+abs(arr[a,2]-arr_c[a])/(3.0))-np.log10(mean10)
                                errmf = -np.log10(mean10-abs(arr[a,2]-arr_c[a])/(3.0))+np.log10(mean10)
                                with open(''.join(('newmeta/g100/dl/',self.net,'_',self.inj, '_', self.a,
                                    '_DEV_log10_', list_names[i], '_', self.k, 'witERR_FAKE.txt')), 'a') as datafile:
                                    datafile.write(''.join((str(np.log10(array_d[a])), '   ', str(mean), '    ', str(np.sqrt(4/5)*abs(arr[a,2]-arr_c[a])/(3.0*arr_c[a]*np.log(10.0))) ,'\n' )))
                                with open(''.join(('newmeta/g100/dl/',self.net,'_',self.inj, '_', self.a,
                                    '_DEV_log10_', list_names[i], '_', self.k, 'witERRlog_FAKE.txt')), 'a') as datafile:
                                    datafile.write(''.join((str(np.log10(array_d[a])), '   ', str(mean), '    ', str(np.nan_to_num(errmf, 0.0)), ' ', str(np.nan_to_num(errpf, 0.0)) ,'\n' )))


                                with open(''.join(('newmeta/g100/dl/',self.net,'_',self.inj, '_', self.a,
                                    '_DEV_log10_', list_names[i], '_', self.k, '_FAKE.txt')), 'a') as datafile:
                                    datafile.write(''.join((str(np.log10(array_d[a])), '   ', str(mean),'\n' )))
                                    print(errmf, errpf, mean+abs(arr[a,2]-arr_c[a]))


                            '''


            array_d1 = []
            arr_c1 = []
            for a in range (0, arr_c.shape[0]):
                if arr_c[a] > 0.0:
                    arr_c1.append(arr_c[a])
                    array_d1.append(array_d[a])
            setup_plot((10,6), 'd', 'SNR')
            #extent=[0,np.maximum(dets_coordinates2[:,:,0,2].reshape((len(decls)*len(ras),))),0,np.maximum(dets_coordinates2[:,:,0,3].reshape((len(decls)*len(ras),)))]
            plt.plot(array_d1, arr_c1, color='red')

            plt.savefig(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_', list_names[i], '_', self.k, '.png')))
            plt.close()

            x3=array_d1
            ys2=arr_c1

            figs, axs = plt.subplots()


            plt.loglog(x3, ys2, label='log-log')
            plt.xlabel('d', fontsize=10)
            plt.ylabel('SNR', fontsize=10)
            plt.tick_params(axis='x', labelsize=10)
            plt.tick_params(axis='y', labelsize=10)
            #plt.legend()
            plt.legend(loc=2, prop={'size': 10})
            figs.tight_layout()
            #plt.show()
            plt.savefig(''.join(('dl_g100feb/ffiles/loglog_',self.net,'_',self.inj, '_', self.k,
                '_DEV_', list_names[i], '_', self.k, '.png')))
            plt.close()

            setup_plot((10,6), 'log(d)', 'log(SNR)')
            #extent=[0,np.maximum(dets_coordinates2[:,:,0,2].reshape((len(decls)*len(ras),))),0,np.maximum(dets_coordinates2[:,:,0,3].reshape((len(decls)*len(ras),)))]
            plt.plot(array_d1, arr_c1, color='red')

            plt.savefig(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_LOG_', list_names[i], '_', self.k, '.png')))
            plt.close()

            setup_plot((10,6), 'log(d)', 'log(SNR)')
            #extent=[0,np.maximum(dets_coordinates2[:,:,0,2].reshape((len(decls)*len(ras),))),0,np.maximum(dets_coordinates2[:,:,0,3].reshape((len(decls)*len(ras),)))]
            plt.plot(array_d1, arr_c1, color='red')

            plt.savefig(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_LOG10_', list_names[i], '_', self.k, '.png')))
            plt.close()

            np.save(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_LOG10_', list_names[i], '_', self.k,'_DATA')), arr_c1)
            np.save(''.join(('dl_g100feb/ffiles/',self.net,'_',self.inj, '_', self.k,
                '_DEV_LOG10_', list_names[i], '_', self.k,'_dists')), array_d1)



        print('Fatto ', self.name)
            #print(arr_c.shape)

            #for k in range (0, 10):

'''
arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_n_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_n_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_n_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_n_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_n_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_n_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_s1_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_s1_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_s1_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_s1_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_s1_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_s1_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdip_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdip_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdip_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdip_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdip_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdip_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sds_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sds_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sds_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sds_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sds_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sds_SNR6')
arr_6_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_v_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_v_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_v_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_v_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_v_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_v_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_w_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_w_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_w_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_w_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_w_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_w_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vw_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vw_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vw_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vw_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vw_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vw_SNR6')
arr_6_hlvk_bbh_s1.get_data()


'''













arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()
'''
arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR6')
arr_6_hlvk_bbh_s1.get_data()
'''

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()







arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_ndl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_ndl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_ndl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_ndl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()
'''
arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR6')
arr_6_hlvk_bbh_s1.get_data()
'''

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_wdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_wdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_wdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_wdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vwdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vwdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vwdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vwdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()







arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_ndl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_ndl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_ndl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_ndl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_ndl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_sdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()
'''
arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sdip_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_sds_SNR6')
arr_6_hlvk_bbh_s1.get_data()
'''

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_wdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_wdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_wdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_wdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_wdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vwdl_3dets')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vwdl_s4dets')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vwdl_v4dets')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_ETCE_dev_vwdl_w4dets')
arr_4_hlvk_bbh_s1.get_data()
#arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_SNR5')
#arr_5_hlvk_bbh_s1.get_data()
#arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVK_dev_vwdl_SNR6')
#arr_6_hlvk_bbh_s1.get_data()












'''
arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_n_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_n_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_n_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_n_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_n_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_n_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_s1_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_s1_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_s1_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_s1_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_s1_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_s1_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdip_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdip_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdip_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdip_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdip_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sdip_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sds_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sds_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sds_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sds_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sds_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_sds_SNR6')
arr_6_hlvk_bbh_s1.get_data()


arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_v_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_v_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_v_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_v_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_v_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_v_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_w_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_w_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_w_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_w_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_w_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_w_SNR6')
arr_6_hlvk_bbh_s1.get_data()

arr_1_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vw_SNR1')
arr_1_hlvk_bbh_s1.get_data()
arr_2_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vw_SNR2')
arr_2_hlvk_bbh_s1.get_data()
arr_3_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vw_SNR3')
arr_3_hlvk_bbh_s1.get_data()
arr_4_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vw_SNR4')
arr_4_hlvk_bbh_s1.get_data()
arr_5_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vw_SNR5')
arr_5_hlvk_bbh_s1.get_data()
arr_6_hlvk_bbh_s1 = DataArr('35.0 0.249_HLVKI_dev_vw_SNR6')
arr_6_hlvk_bbh_s1.get_data()
'''

import numpy as np
import matplotlib.pyplot as plt
import scipy
import time
import sys, os
import re
from mpl_toolkits.axes_grid1 import make_axes_locatable

def interpolate_nan(array_like):
    array = array_like.copy()

    nans = np.isnan(array)

    def get_x(a):
        return a.nonzero()[0]

    array[nans] = np.interp(get_x(nans), get_x(~nans), array[~nans])

    return array



array_m =  np.logspace(0.0, 20, 30, base=1.2 )
#for i in range (0, len(array_m)-1):
#    print(np.log10(array_m[i+1]) - np.log10(array_m[i]))
array_m_log = np.log(array_m)
array_m_log10 = np.log10(array_m)
array_eta1 = np.arange(0.0, 14*0.017 , 0.017)
array_eta = np.zeros_like(array_eta1)
array_eta[:] = 0.249
array_eta[:] -= array_eta1[:]
array_eta = np.flip(array_eta)
#print(array_eta, array_m)
def setup_plot(figsize, xlabel, ylabel):
    """ @brief Initialize basic plot parameters
        @param figsize Figure size
        @param xlabel X-axis label
        @param ylabel Y-axis label
    """

    plt.figure(figsize=figsize)


    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
#print(arr_6_3g_bns_vw.shape)
def heatmap2dlog(arr, str):
    setup_plot((6,2),'Eta', 'Log(Mass)')
    #extent=[0,np.maximum(dets_coordinates2[:,:,0,2].reshape((len(decls)*len(ras),))),0,np.maximum(dets_coordinates2[:,:,0,3].reshape((len(decls)*len(ras),)))]
    plt.imshow(arr, origin='lower', extent=[array_eta[0], array_eta[len(array_eta)-1] , array_m_log[0], array_m_log[len(array_m_log)-1]] , cmap='viridis')

    plt.colorbar()
    #plt.savefig(str)
    plt.close()

    fig, ax = plt.subplots()
    ax.set_box_aspect(1)
    ax.imshow(arr, origin='lower',  cmap='viridis')
    #print(array_eta[0], array_eta[len(array_eta)-1] , array_m_log[0], array_m_log[len(array_m_log)-1])
    plt.savefig(''.join((str, '.png')) )
    plt.close()
def heatmap2dlog10(arr, str):

    fig, ax = plt.subplots()
    #ax.set_box_aspect(arr.shape[0]/arr.shape[1])
    im = ax.imshow(arr, origin='lower', extent=[array_eta[0], array_eta[len(array_eta)-1] , array_m_log10[0], array_m_log10[len(array_m_log10)-1]], cmap='viridis', aspect="auto")
    plt.xlabel('η', fontsize=10)
    plt.ylabel('log(Mc)', fontsize=10)
    plt.tick_params(axis='x', labelsize=10)
    plt.tick_params(axis='y', labelsize=10)
    #plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    fig.colorbar(im, cax=cax)
    fig.tight_layout()
    plt.savefig(''.join((str, '.png')) )
    plt.close()
    #levels = np.linspace(-11, -4, 4 )
    #norm = cm.colors.Normalize(vmax=abs(arr).max(), vmin=-abs(arr).max())
    #cmap = cm.PRGn
    xlist = array_eta
    ylist = array_m
    X, Y = np.meshgrid(xlist, ylist)
    Z = arr
    fig,ax=plt.subplots()
    cp = ax.contourf(X, Y, Z)
    plt.xlabel('η', fontsize=10)
    plt.ylabel('log(Mc)', fontsize=10)
    plt.tick_params(axis='x', labelsize=10)
    plt.tick_params(axis='y', labelsize=10)
    #plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    fig.colorbar(cp, cax=cax) # Add a colorbar to a plot
    #ax.set_title('Filled Contours Plot')
    #ax.set_xlabel('x (cm)')
    #ax.set_ylabel('y (cm)')
    fig.tight_layout()
    plt.savefig(''.join((str, '_cont.png')) )
    plt.close()

def quickcomp(a,b):
    if a/b > 0.99 and a/b < 1.01:
        return True
    else:
        return False
def match(arr, m):
    i = 0
    while i in range (0, len(arr)):
        bool = quickcomp(arr[i], m)
        if bool:
            return int(i)
            break
        i +=1



class DataArr(object):
    def __init__(self, name):

        self.name = str(name)
        self.array = np.full((30,14,4), np.nan)
        string1 = (self.name).split('_')
        self.n=string1[1]
        self.net=string1[0]
        self.obj=string1[2]

    def get_data(self):
        f= open(''.join(('newmeta/', str(self.net) , '_acc_', str(self.obj) ,'.txt')), 'r')

        arr_str=str(f.read()).split("\n")
        arr_str.pop()
        #print(arr_str[0])
        arr = self.array
        print(arr_str[-1])
        for j in range (0, len(arr_str)):
            arr_str[j]=re.split("\s+", arr_str[j])
            for ob in arr_str[j]:
                arr_str[j][arr_str[j].index(ob)] = np.float64(ob)
            #print( np.isnan(arr_str[j]))
                #print(j, "  ok")
            work_arr , arr_str[j] = np.array(arr_str[j]) , np.array(arr_str[j])
            #print(arr_str[j], len(arr_str[j]))
            index_m =  match(array_m, work_arr[1])
            index_eta = match(array_eta, work_arr[0])
            arr[index_m , index_eta , :] = work_arr[2:6]
        arr_str=np.array(arr_str)
        list_names = ['area', 'd_eta' , 'd_Mc', 'd_D']
        file_sav = ''.join(('FILE_SAVE_', self.name))
        for i in range (0,4):
            only_eta=[]
            only_mass=[]
            arr_c = arr[:,:,i]
            for a in range (0, arr_c.shape[0]):
                for b in range (0, arr_c.shape[1]):
                    if not np.isnan(arr_c[a,b]):
                        if b ==(arr_c.shape[1] - 1):
                            only_mass.append(arr_c[a,b])
                            with open(''.join(('newmeta/',list_names[i], '_', file_sav, '_only_mass.txt')), 'a') as datafile:
                                datafile.write(''.join((str(array_m[a]), '  ',str(arr_c[a,b]), '\n')))
                        if a ==(arr_c.shape[0] - 1):
                            only_eta.append(arr_c[a,b])
                            with open(''.join(('newmeta/',list_names[i], '_', file_sav, '_only_eta.txt')), 'a') as datafile:
                                datafile.write(''.join((str(array_eta[b]), '  ',str(arr_c[a,b]), '\n')))

                        with open(''.join(('newmeta/',list_names[i], '_', file_sav, '_newmeta.txt')), 'a' ) as datafile:
                            datafile.write(''.join((str(array_m[a]), '  ',str(array_eta[b]), '  ',str(arr_c[a,b]), '\n')))




prova = DataArr('3G_acc_errs')
prova.get_data()
prova = DataArr('INDIA_acc_errs')
prova.get_data()
prova = DataArr('OF_acc_errs')
prova.get_data()

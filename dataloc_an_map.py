import numpy as np
import matplotlib.pyplot as plt
import scipy
import time
import sys, os
import re
from mpl_toolkits.axes_grid1 import make_axes_locatable

def interpolate_nan(array_like):
    array = array_like.copy()

    nans = np.isnan(array)

    def get_x(a):
        return a.nonzero()[0]

    array[nans] = np.interp(get_x(nans), get_x(~nans), array[~nans])

    return array



array_m =  np.logspace(0.0, 20, 30, base=1.2 )
#for i in range (0, len(array_m)-1):
#    print(np.log10(array_m[i+1]) - np.log10(array_m[i]))
array_m_log = np.log(array_m)
array_m_log10 = np.log10(array_m)
array_eta1 = np.arange(0.0, 14*0.017 , 0.017)
array_eta = np.zeros_like(array_eta1)
array_eta[:] = 0.249
array_eta[:] -= array_eta1[:]
array_eta = np.flip(array_eta)
#print(array_eta, array_m)
def setup_plot(figsize, xlabel, ylabel):
    """ @brief Initialize basic plot parameters
        @param figsize Figure size
        @param xlabel X-axis label
        @param ylabel Y-axis label
    """

    plt.figure(figsize=figsize)


    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
#print(arr_6_3g_bns_vw.shape)
def heatmap2dlog(arr, str):
    setup_plot((10,6),'RA', 'DEC')
    #extent=[0,np.maximum(dets_coordinates2[:,:,0,2].reshape((len(decls)*len(ras),))),0,np.maximum(dets_coordinates2[:,:,0,3].reshape((len(decls)*len(ras),)))]
    plt.imshow(arr, origin='lower', extent=[0, 360 , -90, 90] , cmap='viridis')

    plt.colorbar()
    #plt.savefig(str)

    #print(array_eta[0], array_eta[len(array_eta)-1] , array_m_log[0], array_m_log[len(array_m_log)-1])
    plt.savefig(''.join((str, '.png')) )
    plt.close()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='mollweide')


    lon = np.linspace(-np.pi, np.pi,90)
    lat = np.linspace(-np.pi/2., np.pi/2.,45)
    Lon,Lat = np.meshgrid(lon,lat)

    im = ax.pcolormesh(Lon,Lat,arr, cmap=plt.cm.jet, shading='auto')
    plt.colorbar(im)
    plt.savefig(''.join((str, 'moll.png')) )

def heatmap2dlog10(arr, str):

    fig, ax = plt.subplots()
    #ax.set_box_aspect(arr.shape[0]/arr.shape[1])
    im = ax.imshow(arr, origin='lower', extent=[array_eta[0], array_eta[len(array_eta)-1] , array_m_log10[0], array_m_log10[len(array_m_log10)-1]], cmap='viridis', aspect="auto")
    plt.xlabel('η', fontsize=10)
    plt.ylabel('log(Mc)', fontsize=10)
    plt.tick_params(axis='x', labelsize=10)
    plt.tick_params(axis='y', labelsize=10)
    #plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    fig.colorbar(im, cax=cax)
    fig.tight_layout()
    plt.savefig(''.join((str, '.png')) )
    plt.close()
    #levels = np.linspace(-11, -4, 4 )
    #norm = cm.colors.Normalize(vmax=abs(arr).max(), vmin=-abs(arr).max())
    #cmap = cm.PRGn
    xlist = array_eta
    ylist = array_m
    X, Y = np.meshgrid(xlist, ylist)
    Z = arr
    fig,ax=plt.subplots()
    cp = ax.contourf(X, Y, Z)
    plt.xlabel('η', fontsize=10)
    plt.ylabel('log(Mc)', fontsize=10)
    plt.tick_params(axis='x', labelsize=10)
    plt.tick_params(axis='y', labelsize=10)
    #plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    fig.colorbar(cp, cax=cax) # Add a colorbar to a plot
    #ax.set_title('Filled Contours Plot')
    #ax.set_xlabel('x (cm)')
    #ax.set_ylabel('y (cm)')
    fig.tight_layout()
    plt.savefig(''.join((str, '_cont.png')) )
    plt.close()

def quickcomp(a,b):
    if abs(a-b) > -0.01 and abs(a-b) < 0.01:
        return True
    else:
        return False
def match(arr, m):
    i = 0
    while i in range (0, len(arr)):
        bool = quickcomp(float(arr[i]), float(m))
        if bool:
            return int(i)
            break
        i +=1



class DataArr(object):
    def __init__(self, name):

        self.name = str(name)
        #self.array = np.full((30,14,4), np.nan)
        string1 = (self.name).split('_')
        self.n=string1[1]
        self.net=string1[0]
        self.obj=string1[2]
        r=np.zeros((45,90))
    def get_data(self):
        f= open(''.join(('newmeta/g100/', str(self.net) , '_accNUOVO.txt')), 'r')
        ras = np.arange(0.0,90.0)
        decs = np.arange(0.0,45.0)
        arr_str=str(f.read()).split("\n")
        arr_str.pop()
        #print(arr_str[0])
        arr = np.zeros((45,90))
        print(arr_str[-1])
        for j in range (0, len(arr_str)):
            arr_str[j]=re.split("\s+", arr_str[j])
            arr_str[j].pop()
            for ob in arr_str[j]:
                #print(ob, np.float64(ob))
                arr_str[j][arr_str[j].index(ob)] = np.float64(ob)
            #print( np.isnan(arr_str[j]))
                #print(j, "  ok")
            work_arr , arr_str[j] = np.array(arr_str[j]) , np.array(arr_str[j])
            #print(arr_str[j], len(arr_str[j]))
            print(len(work_arr))
            index_ra =  match(ras, 180*work_arr[2]/(4*np.pi))
            print('ra ',index_ra)
            index_dec = match(decs, 180*work_arr[0]/(4*np.pi))
            print('dec ',index_dec, '   ', 180*work_arr[0]/(4*np.pi))
            arr[index_dec , index_ra ] = work_arr[1]
        arr=np.array(arr)
        #list_names = ['area', 'd_eta' , 'd_Mc', 'd_D']
        file_sav = ''.join(('FILE_SAVE_', self.name))


        #for a in range (0, arr.shape[0]):
        #    for b in range (0, arr.shape[1]):

        #        with open(''.join(('newmeta/',list_names[i], '_', file_sav, '_newmeta.txt')), 'a' ) as datafile:
    #                datafile.write(''.join((str(array_m[a]), '  ',str(array_eta[b]), '  ',str(arr_c[a,b]), '\n')))

        heatmap2dlog(arr, ''.join(('newmeta/MAPPA_', self.net)))
        #heatmap2dlog10(arr, ''.join(('newmeta/MAPPA', self.name)))
        print('Fatto ', self.name)


prova = DataArr('3G_acc_errs')
prova.get_data()
prova = DataArr('INDIA_acc_errs')
prova.get_data()
prova = DataArr('OF_acc_errs')
prova.get_data()

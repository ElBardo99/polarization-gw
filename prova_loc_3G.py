#!/usr/bin/env python
# coding: utf-8

import time
import multiprocessing
import sys, os
import numpy as np
from gwbench import network
from gwbench import basic_relations as br
import matplotlib.pyplot as plt
from gwbench import io_mod as io
import scipy
import scipy.signal
from gwbench.basic_constants import Mpc, Msun, GNewton, cLight
def ChirpTime(fs, Mc):
    Mc *= GNewton/cLight**3 * Msun
    fn = np.asarray([fs]) if np.isscalar(fs) else np.asarray(fs)
    return ((5./256.) / (Mc**(5./3.) * (np.pi * fn)**(8./3.)))

def funz(y, a, b):
    return [y**3 + 2*b*(1-y**2)-y*(1+a**2), 3*y**2 -2*b*2*y-(1+a**2)]
asse=np.arange(-1, 1, 0.001)


def zeri(fp, fc, psi, thresh=0.005):
    a=fp*np.sin(2*psi)+fc*np.cos(2*psi)
    b=-fp*np.cos(2*psi)+fc*np.sin(2*psi)
    for ips in np.flip(np.arange (-1.0, 1.0, 0.1)):
        back=-9999.9
        while (abs(funz(ips, a, b)[0]) >= thresh and abs(ips-back)>=thresh):
            back=ips
            ips=ips-(funz(ips,a,b)[0]/funz(ips,a,b)[1])
            #print(ips)
        if abs(ips)<1.0:
            break
    return [ips, np.sqrt((a**2)/(1-ips**2))]

def fscalar(fplus, fcross, psi):
    r = (1-fcross)/np.sin(2*psi) + (fplus*np.sin(2*psi) + fcross*np.cos(2*psi))/np.tan(2*psi)
    s = (1+fplus)/np.cos(2*psi) - (fplus*np.sin(2*psi) + fcross*np.cos(2*psi))*np.tan(2*psi)
    return [r,s]

def fx(theta, phi, psi):
    res=-np.sin(theta)*np.sin(2*phi)*np.cos(psi) + np.sin(theta)*np.cos(theta)*np.sin(psi)*np.cos(2*phi)
    return res

def fy(theta, phi, psi):
    res=np.sin(theta)*np.sin(2*phi)*np.sin(psi) + np.sin(theta)*np.cos(theta)*np.cos(psi)*np.cos(2*phi)
    return res

def fscal(theta, phi, psi):
    res=0.5*(np.sin(theta)**2)*np.cos(2*phi)
    return res

def setup_plot(figsize, xlabel, ylabel):
    """ @brief Initialize basic plot parameters
        @param figsize Figure size
        @param xlabel X-axis label
        @param ylabel Y-axis label
    """

    plt.figure(figsize=figsize)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
def heatmap2d(arr, str):
    setup_plot((10,6), 'RA', 'DEC')
    #extent=[0,np.maximum(dets_coordinates2[:,:,0,2].reshape((len(decls)*len(ras),))),0,np.maximum(dets_coordinates2[:,:,0,3].reshape((len(decls)*len(ras),)))]
    plt.imshow(arr, origin='lower', extent=[0, 360, -90, 90] , cmap='viridis')

    plt.colorbar()
    plt.savefig('mappa_3G.png')
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='mollweide')


    lon = np.linspace(-np.pi, np.pi,90)
    lat = np.linspace(-np.pi/2., np.pi/2.,45)
    Lon,Lat = np.meshgrid(lon,lat)

    im = ax.pcolormesh(Lon,Lat,arr, cmap=plt.cm.jet, shading='auto')
    plt.colorbar(im)
    plt.savefig('mappa_3G_mollweide.png')

decls=np.arange(-np.pi/2, np.pi/2, np.pi/180)

ras=np.arange(0., 2*np.pi, np.pi/180)
from scipy.integrate import quad
distanza_l=100.0
Mc=50
def integr(zi):
    Ho=68.0
    c=300000.0
    Om=0.3
    Ok=0.0
    res=(c/Ho)*1/(np.sqrt(Om*(1+zi)**3 +  1-Om))
    return res
def fun(z):

    Lsky=quad(integr,0,z,args=())[0]
    zer=distanza_l - (1+z)*Lsky
    return zer

redshifted=1
if redshifted:
    z_vec=scipy.optimize.root_scalar(fun, bracket=[0.0, 3]).root
    #print(z_vec)
    Mc *= (1. + z_vec)

network_spec = ['ET_ET1','ET_ET2','ET_ET3', 'CE2-20-CBO_N', 'CE2-40-CBO_S']
network_spec2 = ['A+_H', 'A+_L', 'A+_V', 'A+_K']

dets={
    'A+_H': 0,
    'A+_L': 1,
    'A+_V': 2,
    'A+_K': 3

}
file = open("gwbench/noise_curves/a_plus.txt", "r")

arr_str=str(file.read()).split("\n")
#print(arr_str[0])
for i in range (0, len(arr_str)):
    arr_str[i]=arr_str[i].split(' ')
arr_str.pop()
for i in range (0, len(arr_str)):
    #print(i)
    for j in (0, 1):
        arr_str[i][j]=float(arr_str[i][j])
arr_str=np.array(arr_str)
#print(arr_str)
noise_ce=np.transpose(arr_str)

noise_ce[0,0]=5

sys.stdout.flush()




cos = np.cos
sin = np.sin
exp = np.exp
PI = np.pi
from gwbench.basic_constants import time_fac, REarth, AU, cLight
def rrot_mat(angle,axis):
    c = np.cos(angle)
    s = np.sin(angle)

    if axis == 'i':
        return np.array( ( (1,0,0), (0,c,-s), (0,s,c) ) )
    if axis == 'j':
        return np.array( ( (c,0,s), (0,1,0), (-s,0,c) ) )
    if axis == 'k':
        return np.array( ( (c,-s,0), (s,c,0), (0,0,1) ) )

def ddet_angles(loc):
    # return alpha, beta, gamma in radians
    # alpha ... longitude
    # beta  ... pi/2 - latitude
    # gamma ... angle from 'Due East' to y-arm
    if loc == 'H':
        return -2.08406, PI/2.-0.810795, PI-5.65488
    elif loc == 'L':
        return -1.58431, PI/2.-0.533423, PI-4.40318
    elif loc in ('V','ET1','ET2','ET3'):
        return 0.183338, PI/2.-0.761512, PI-0.33916
    elif loc == 'K':
        return 2.3942, PI/2.-0.632682, PI-1.054113
    elif loc == 'I':
        return 1.334013, PI/2.-0.248418, PI-1.570796

    elif loc == 'C':
        return -1.969174, PI/2.-0.764918, 0.
    elif loc == 'N':
        return -2.1817, PI/2.-0.8029, 3*PI/4.
    elif loc == 'S':
        return -1.64061, PI/2.-0.50615, 0.

def ddet_angles_NEW(loc):
    # return alpha, beta, gamma in radians
    # alpha ... longitude
    # beta  ... pi/2 - latitude
    # gamma ... angle from 'Due East' to y-arm
    if loc == 'H':
        return -2.08406, PI/2.-0.810795, PI-5.65488
    elif loc == 'L':
        return -1.58431, PI/2.-0.533423, PI-4.40318
    elif loc in ('V','ET1','ET2','ET3'):
        return 0.183338, PI/2.-0.761512, PI-0.33916
    elif loc == 'K':
        return 2.3942, PI/2.-0.632682, PI-1.054113
    elif loc == 'I':
        return 1.334013, PI/2.-0.248418, PI-1.570796

    elif loc == 'C':
        return -1.969174, PI/2.-0.764918, 0.
    elif loc == 'N':
        return -2.1817, PI/2.-0.8029, 3*PI/4.
    elif loc == 'S':
        return -1.64061, PI/2.-0.50615, 0.

def ddet_ten_and_loc_vec(loc, R):
    i_vec = np.array((1,0,0))
    j_vec = np.array((0,1,0))
    k_vec = np.array((0,0,1))

    et_vec2 = ( i_vec + np.sqrt(3.)*j_vec)/2.
    et_vec3 = (-i_vec + np.sqrt(3.)*j_vec)/2.

    alpha, beta, gamma = ddet_angles(loc)
    EulerD1 = np.matmul(np.matmul(rrot_mat(alpha,'k'), rrot_mat(beta,'j')), rrot_mat(gamma,'k'))

    if loc in   ('ET3','LISA3'):
        eDArm1 = -1 * np.matmul(EulerD1,et_vec2)
        eDArm2 = -1 * np.matmul(EulerD1,et_vec3)
    elif loc in ('ET2','LISA2'):
        eDArm1 =      np.matmul(EulerD1,et_vec3)
        eDArm2 = -1 * np.matmul(EulerD1,i_vec)
    elif loc in ('ET1','LISA1'):
        eDArm1 =      np.matmul(EulerD1,i_vec)
        eDArm2 =      np.matmul(EulerD1,et_vec2)
    else:
        eDArm1 = np.matmul(EulerD1,i_vec)
        eDArm2 = np.matmul(EulerD1,j_vec)

    return np.outer(eDArm1,eDArm1) - np.outer(eDArm2,eDArm2), R/cLight * np.matmul(EulerD1,k_vec)


def ddet_ten_and_loc_vec_NEW(loc, R):
    i_vec = np.array((1,0,0))
    j_vec = np.array((0,1,0))
    k_vec = np.array((0,0,1))

    et_vec2 = ( i_vec + np.sqrt(3.)*j_vec)/2.
    et_vec3 = (-i_vec + np.sqrt(3.)*j_vec)/2.

    alpha, beta, gamma = ddet_angles_NEW(loc)
    EulerD1 = np.matmul(np.matmul(rrot_mat(alpha,'k'), rrot_mat(beta,'j')), rrot_mat(gamma,'k'))

    if loc in   ('ET3','LISA3'):
        eDArm1 = -1 * np.matmul(EulerD1,et_vec2)
        eDArm2 = -1 * np.matmul(EulerD1,et_vec3)
    elif loc in ('ET2','LISA2'):
        eDArm1 =      np.matmul(EulerD1,et_vec3)
        eDArm2 = -1 * np.matmul(EulerD1,i_vec)
    elif loc in ('ET1','LISA1'):
        eDArm1 =      np.matmul(EulerD1,i_vec)
        eDArm2 =      np.matmul(EulerD1,et_vec2)
    else:
        eDArm1 = np.matmul(EulerD1,i_vec)
        eDArm2 = np.matmul(EulerD1,j_vec)

    return np.outer(eDArm1,eDArm1) - np.outer(eDArm2,eDArm2), R/cLight * np.matmul(EulerD1,k_vec)

def apkons(ra,dec,t,Mc,tc,psi,gmst0,loc,use_rot, time):
    half_period = 4.32e4
    R = REarth

    D, d = ddet_ten_and_loc_vec(loc, R)

    if use_rot:
        if time:
            tf = t
        else:
            tf= tc - (5./256.)*(time_fac*Mc)**(-5./3.)*(PI*t)**(-8./3.)
    else:
        tf = 0

    gra = (gmst0 + tf*PI/half_period) - ra
    #print(type(gra))
    theta = PI/2. - dec

    if isinstance(gra, np.ndarray):
        r = np.array((cos(gra) * sin(theta), sin(gra) * sin(theta), cos(theta) * np.ones(len(gra))))
        XX = np.transpose(np.array([ -cos(psi)*sin(gra) - sin(psi)*cos(gra)*sin(dec), -cos(psi)*cos(gra) + sin(psi)*sin(gra)*sin(dec), sin(psi)*cos(dec) * np.ones(len(gra)) ]))
        YY = np.transpose(np.array([  sin(psi)*sin(gra) - cos(psi)*cos(gra)*sin(dec),  sin(psi)*cos(gra) + cos(psi)*sin(gra)*sin(dec), cos(psi)*cos(dec) * np.ones(len(gra)) ]))
        ZZ = np.transpose(np.array([  -cos(gra)*cos(dec) , cos(dec)*sin(gra) , -sin(dec)* np.ones(len(gra))]))
        Fp = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),XX[i]) - np.matmul(np.matmul(YY[i],D),YY[i]) for i in range(len(gra))])
        Fc = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),YY[i]) + np.matmul(np.matmul(YY[i],D),XX[i]) for i in range(len(gra))])
        Fs1 = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),XX[i]) + np.matmul(np.matmul(YY[i],D),YY[i]) for i in range(len(gra))])
        Fs2 = 0.5 * np.array([np.matmul(np.matmul(ZZ[i],D),ZZ[i]) for i in range(len(gra))])
        Fv = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),ZZ[i]) + np.matmul(np.matmul(ZZ[i],D),XX[i]) for i in range(len(gra))])
        Fw = 0.5 * np.array([np.matmul(np.matmul(YY[i],D),ZZ[i]) + np.matmul(np.matmul(YY[i],D),ZZ[i]) for i in range(len(gra))])
    else:
        r = np.array((cos(gra) * sin(theta), sin(gra) * sin(theta), cos(theta)))
        XX = np.transpose(np.array([ -cos(psi)*sin(gra) - sin(psi)*cos(gra)*sin(dec), -cos(psi)*cos(gra) + sin(psi)*sin(gra)*sin(dec), sin(psi)*cos(dec) ]))
        YY = np.transpose(np.array([  sin(psi)*sin(gra) - cos(psi)*cos(gra)*sin(dec),  sin(psi)*cos(gra) + cos(psi)*sin(gra)*sin(dec), cos(psi)*cos(dec) ]))
        ZZ = np.transpose(np.array([  -cos(gra)*cos(dec) , cos(dec)*sin(gra) , -sin(dec)]))
        Fp = 0.5 * (np.matmul(np.matmul(XX,D),XX) - np.matmul(np.matmul(YY,D),YY))
        Fc = 0.5 * (np.matmul(np.matmul(XX,D),YY) + np.matmul(np.matmul(YY,D),XX))
        Fs1 = 0.5 * (np.matmul(np.matmul(XX,D),XX) + np.matmul(np.matmul(YY,D),YY))
        Fs2 = 0.5 * (np.matmul(np.matmul(ZZ,D),ZZ))
        Fv = 0.5 * (np.matmul(np.matmul(XX,D),ZZ) + np.matmul(np.matmul(ZZ,D),XX))
        Fw = 0.5 * (np.matmul(np.matmul(YY,D),ZZ) + np.matmul(np.matmul(ZZ,D),YY))
    #print(len(Fp))
    return Fp, Fc, Fv , Fw , Fs1 , Fs2

def apkons_NEW(ra,dec,t,Mc,tc,psi,gmst0,loc,use_rot, time):
    half_period = 4.32e4
    R = REarth

    D, d = ddet_ten_and_loc_vec_NEW(loc, R)

    if use_rot:
        if time:
            tf = t
        else:
            tf= tc - (5./256.)*(time_fac*Mc)**(-5./3.)*(PI*t)**(-8./3.)
    else:
        tf = 0

    gra = (gmst0 + tf*PI/half_period) - ra
    theta = PI/2. - dec

    if isinstance(gra, np.ndarray):
        r = np.array((cos(gra) * sin(theta), sin(gra) * sin(theta), cos(theta) * np.ones(len(gra))))
        XX = np.transpose(np.array([ -cos(psi)*sin(gra) - sin(psi)*cos(gra)*sin(dec), -cos(psi)*cos(gra) + sin(psi)*sin(gra)*sin(dec), sin(psi)*cos(dec) * np.ones(len(gra)) ]))
        YY = np.transpose(np.array([  sin(psi)*sin(gra) - cos(psi)*cos(gra)*sin(dec),  sin(psi)*cos(gra) + cos(psi)*sin(gra)*sin(dec), cos(psi)*cos(dec) * np.ones(len(gra)) ]))
        ZZ = np.transpose(np.array([  -cos(gra)*cos(dec) , cos(dec)*sin(gra) , -sin(dec)* np.ones(len(gra))]))
        Fp = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),XX[i]) - np.matmul(np.matmul(YY[i],D),YY[i]) for i in range(len(gra))])
        Fc = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),YY[i]) + np.matmul(np.matmul(YY[i],D),XX[i]) for i in range(len(gra))])
        Fs1 = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),XX[i]) + np.matmul(np.matmul(YY[i],D),YY[i]) for i in range(len(gra))])
        Fs2 = 0.5 * np.array([np.matmul(np.matmul(ZZ[i],D),ZZ[i]) for i in range(len(gra))])
        Fv = 0.5 * np.array([np.matmul(np.matmul(XX[i],D),ZZ[i]) + np.matmul(np.matmul(ZZ[i],D),XX[i]) for i in range(len(gra))])
        Fw = 0.5 * np.array([np.matmul(np.matmul(YY[i],D),ZZ[i]) + np.matmul(np.matmul(YY[i],D),ZZ[i]) for i in range(len(gra))])
    else:
        r = np.array((cos(gra) * sin(theta), sin(gra) * sin(theta), cos(theta)))
        XX = np.transpose(np.array([ -cos(psi)*sin(gra) - sin(psi)*cos(gra)*sin(dec), -cos(psi)*cos(gra) + sin(psi)*sin(gra)*sin(dec), sin(psi)*cos(dec) ]))
        YY = np.transpose(np.array([  sin(psi)*sin(gra) - cos(psi)*cos(gra)*sin(dec),  sin(psi)*cos(gra) + cos(psi)*sin(gra)*sin(dec), cos(psi)*cos(dec) ]))
        ZZ = np.transpose(np.array([  -cos(gra)*cos(dec) , cos(dec)*sin(gra) , -sin(dec)]))
        Fp = 0.5 * (np.matmul(np.matmul(XX,D),XX) - np.matmul(np.matmul(YY,D),YY))
        Fc = 0.5 * (np.matmul(np.matmul(XX,D),YY) + np.matmul(np.matmul(YY,D),XX))
        Fs1 = 0.5 * (np.matmul(np.matmul(XX,D),XX) + np.matmul(np.matmul(YY,D),YY))
        Fs2 = 0.5 * (np.matmul(np.matmul(ZZ,D),ZZ))
        Fv = 0.5 * (np.matmul(np.matmul(XX,D),ZZ) + np.matmul(np.matmul(ZZ,D),XX))
        Fw = 0.5 * (np.matmul(np.matmul(YY,D),ZZ) + np.matmul(np.matmul(ZZ,D),YY))

    return Fp, Fc, Fv , Fw , Fs1 , Fs2

def kons(ra,decl,psi, t, Mc, tc, gmst0, loc, use_rot, time):
    p=np.zeros((4,))
    c=np.zeros((4,))
    s1=np.zeros((4,))
    s2=np.zeros((4,))
    v=np.zeros((4,))
    w=np.zeros((4,))
    lp=[0.0]*4

    gmst0=inj_params['gmst0']
    Mc=inj_params['Mc']
    tc=inj_params['tc']
    #psi=inj_params['psi']
    gmst0=inj_params['gmst0']
    use_rot=False

    for dec in range(0, len(network_spec2)):
        loc1=str(network_spec2[dec]).split('_')
        loc=loc1[1]
        p[dec], c[dec] , v[dec] , w[dec],  s1[dec] , s2[dec] = apkons(ra, decl, t, Mc, tc, psi, gmst0, loc, use_rot, time)




    return p, c, v, w, s1



bool_scal=0
bool_vec=0
As=1
Aw=1
Av=1
use_rot=1



network_spec5=['ET_ET1','ET_ET2','ET_ET3','CE2-20-CBO_N', 'CE2-40-CBO_S']
network_spec6 = ['A+_H' , 'A+_L' , 'A+_V' , 'A+_K' , 'A+_I']
network_spec7= ['A+_H', 'A+_L', 'A+_V', 'A+_K']
#network_spec2 = ['ET_ET1','A+_H', 'A+_L', 'V+_V', 'K+_K']



Mc_bbh=35.0

redshifted=1
#Mc_vec=Mc
if redshifted:
    z_vec=scipy.optimize.root_scalar(fun, bracket=[0.0, 3]).root
    print(z_vec)
    Mc_bbh *= (1. + z_vec)


def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint():
    sys.stdout = sys.__stdout__

r = np.zeros((45,90))
def rafun(ra):
#for ra in range (0, 90):

    for dec in range (0, 45):
        print('ra: ', ra, 'dec: ', dec)
        inj_params_bbh = {
            'Mc':    Mc_bbh,
            'eta':   0.249,
            'chi1z': 0,
            'chi2z': 0,
            'DL':    distanza_l,
            'tc':    0,
            'phic':  0,
            'iota':  np.pi/4,
            'ra':    4*ra*np.pi/180.0,
            'dec':   4*dec*np.pi/180.0,
            'psi':   np.pi/4,
            'gmst0': 0
        }
        #Mtot=br.M_of_Mc_eta(35,0.24)
        p=1
        #Mtot_td=br.M_of_Mc_eta(inj_params['Mc'],inj_params['eta'])

        f_lo=3.1

        f_hi_bbh = br.f_isco_Msolar(br.M_of_Mc_eta(inj_params_bbh['Mc'], inj_params_bbh['eta']))
        tc_bbh = ChirpTime(f_lo, inj_params_bbh['Mc'])

        sys.stdout.flush()

        f_hi1=4096.0/2.0
        deltaT = 1/(2*f_hi1)
        nts_bbh = int(2**(np.ceil(np.log2(tc_bbh/deltaT))+p))
        df_bbh = 2**-4

        f_bbh=np.arange(f_lo, f_hi_bbh, df_bbh)
        t_bbh=np.linspace(-tc_bbh, 0, 2*int(f_hi1/df_bbh)-1)



        #print(2*int(f_hi/df)-1, len(t))
        sys.stdout.flush()

        dets_try={
            'A+_H': 0,
            'A+_L': 1,
            'A+_V': 2,
            'A+_K': 3
        }

        deriv_symbs_string = 'Mc eta DL chi1z chi2z tc phic iota ra dec psi'
        conv_cos = ('')
        conv_log = ('')
        use_rot = 0
        blockPrint()
        df1=2**-4
        f_prova_bbh=np.arange(f_lo, f_hi_bbh, df1)
        t_prova = np.linspace(-tc_bbh, 0, 2*int(f_hi1/df1)-1)
        net_tdp_bbh = network.Network(network_spec5)
        net_tdp_bbh.set_wf_vars(wf_model_name='tf2')
        net_tdp_bbh.set_net_vars(f=f_prova_bbh, inj_params=inj_params_bbh, deriv_symbs_string=deriv_symbs_string,
                            conv_cos=conv_cos, conv_log=conv_log, use_rot=use_rot)
        net_tdp_bbh.calc_wf_polarizations()
        net_tdp_bbh.calc_wf_polarizations_derivs_num()
        net_tdp_bbh.setup_ant_pat_lpf_psds()
        net_tdp_bbh.calc_det_responses()
        net_tdp_bbh.calc_det_responses_derivs_num()
        net_tdp_bbh.calc_snrs()
        net_tdp_bbh.calc_errors(cond_sup=np.inf)
        net_tdp_bbh.calc_sky_area_90()
        sys.stdout.flush()
        ntot=10
        d1_bbh=np.sqrt(net_tdp_bbh.cov[8,8])
        d2_bbh=np.sqrt(net_tdp_bbh.cov[9,9])

        rdr=net_tdp_bbh.errs['sky_area_90']
        enablePrint()
        print(rdr)
        with open("3G_accNUOVO.txt", 'a') as datafile:
            datafile.write(''.join((str(4*dec*np.pi/180.0), '   ' , str(4*ra*np.pi/180.0), '   ' , str(net_tdp_bbh.cov[8,8]), '   ' , str(net_tdp_bbh.cov[8,9]), '   ' , str(net_tdp_bbh.cov[9,9]), '   ' , '\n')))
    np.save(''.join(('maps_files/3G_', str(ra))), rdr)

if __name__ == '__main__':
    starttime = time.time()
    processes = []
    for ra in range(0,90):
        p2 = multiprocessing.Process(target=rafun, args=(ra,))
        processes.append(p2)
        p2.start()

    for process in processes:
        process.join()
for ra in range(0,90):
    r[:,ra] = np.load(file=''.join(('maps_files/3G_', str(ra), '.npy')))
    os.remove(''.join(('maps_files/3G_', str(ra), '.npy')))
heatmap2d(r, 'mappa_3GNUOVO.png')

#!/bin/bash
#SBATCH --job-name=nminus_meta1_1000
#SBATCH -p g100_usr_prod # partition (queue)
#SBATCH -N 1 # number of nodes
#SBATCH --ntasks-per-node=1 # 10 tasks out of 32
#SBATCH --mem 300G  # memory pool for all cores
#SBATCH -t 02:00:00 # time
#SBATCH -o outs/nminus_meta_1000_%a.out # STDOUT
#SBATCH -e errs/nminus_meta_1000_%a.err # STDERR
#SBATCH -A uTS24_Troian 
#SBATCH --array=0-109:1
#SBATCH --verbose
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail         # send email if job fails
#SBATCH --mail-user='giuseppetroian99@gmail.com'
A=$((SLURM_ARRAY_TASK_ID/14))
B=$((SLURM_ARRAY_TASK_ID%14))
srun metatensor_minus_1000.py $A $B


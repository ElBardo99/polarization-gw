#!/bin/bash
#SBATCH --job-name=w_dl
#SBATCH -p g100_usr_prod # partition (queue)
#SBATCH -N 1 # number of nodes
#SBATCH --ntasks-per-node=1 # 10 tasks out of 32
#SBATCH --mem 300G  # memory pool for all cores
#SBATCH -t 00:15:00 # time
#SBATCH -o outs/vecw_dl_%a.out # STDOUT
#SBATCH -e errs/vecw_dl_%a.err # STDERR
#SBATCH -A uTS24_Troian 
#SBATCH --array=0-29:1
#SBATCH --verbose
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail         # send email if job fails
#SBATCH --mail-user='giuseppetroian99@gmail.com'
srun tensor_w.py ${SLURM_ARRAY_TASK_ID}

